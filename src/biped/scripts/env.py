import sys
sys.path.append('../../brendan')
import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import pybullet as p
import gym
from gym import spaces
import time
import numpy as np
from numpy import cos as c
from numpy import sin as s
from gym.utils import seeding
from collections import deque
import cv2
import time
from obstacles import Obstacles
import copy
import random
from mpi4py import MPI

class Env(gym.Env):
    # 44 state, 1 vx control
    ob_size = 45
    ac_size = 12
    timeStep = 0.005
    initial_reward_flag = True
    initial_reward = 0
    total_reward = 0
    Kp = Kd = 400
    Kd_x = Kd
    steps = 0
    total_steps = 0
    actionRepeat = 2
    box_info = None
    height_coeff = 0.0
    # height_coeff = 0.1
    step_length = 0.3
    speed = 1.25
    yaw_v = 0
    yawing = False
    speed_range = 0
    mode = 'flat'
    # height_coeff = 0.1
    frict_coeff = 0.0
    # length_coeff = 0.0
    length_coeff = 0.3
    fov = 60
    nearPlane = 0.01
    farPlane = 1000
    aspect = 1
    episodes = -1
    max_v = 2.0
    min_v = 0.0
    max_yaw = 0.75
    ep_lens = deque(maxlen=5)
    projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane);
    def __init__(self):
        pass
    def arguments(self, RENDER=False, PATH=None, MASTER=True, cur=False, down_stairs=False, up_stairs=False, disturbances=False, record_step=True, camera_rotation=80, variable_start=False, add_flat=False, colour=False, y_offset=0.0, z_offset=0.0, speed_cur=False, other_rewards = False, control=True, up_down_flat=False, mh=False, motor_control='torque', sym=False):
        # if up_stairs or down_stairs:
        #     self.control = False
        # else:
        self.motor_control = motor_control
        self.control = control
        if self.control:
            self.ob_size = 46
        else:
            self.ob_size = 44
        high = 5*np.ones(self.ac_size)
        low = -high
        self.action_space  = spaces.Box(low, high)

        high = np.inf*np.ones(self.ob_size)
        low = -high
        self.observation_space = spaces.Box(low,high)
        # self.arguments(False, False, '4')
        self.colour = colour
        self.render = RENDER
        self.record_step = record_step
        self.camera_rotation = camera_rotation
        self.PATH = PATH
        self.cur = cur
        self.sym = sym
        self.down_stairs = down_stairs
        self.up_stairs = up_stairs
        self.speed_cur = speed_cur
        self.other_rewards = other_rewards
        self.disturbances = disturbances
        self.variable_start = variable_start
        self.up_down_flat = up_down_flat
        self.add_flat = add_flat
        self.y_offset = y_offset
        self.z_offset = z_offset
        self.mh = mh
        self.obstacles = Obstacles(y_offset=self.y_offset, add_friction=None, colour=self.colour)
        self.MASTER = MASTER
        if self.render and self.MASTER:
        	self.physicsClientId = p.connect(p.GUI)
        else:
            self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the running gait
        self.sim_data = []
        self.best_reward = -100

        # Continuous values for normalising ====================================
        # Right and Left Joint positions, velocities, feet contacts, feet positions
        self.state = ['right_hip_x_pos','right_hip_z_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
        self.state += ['left_hip_x_pos', 'left_hip_z_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
        self.state += ['right_hip_x_vel','right_hip_z_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
        self.state += ['left_hip_x_vel', 'left_hip_z_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']

        self.state += ['com_z']
        self.state += ['vx','vz','pitch','pitch_vel']

        # Needs negatives when inverting:
        self.state += ['vy','roll','yaw']
        self.state += ['roll_vel','yaw_vel']

        # Not continous values, don't normalise ================================

        self.state += ['prev_right_foot_left_ground','right_foot_left_ground']
        self.state += ['prev_left_foot_left_ground','left_foot_left_ground']

        # Previous trajectory targets
        # self.state += ['ready_to_walk']
        # Y states, and roll and yaw that need to be negatived..
        # States that need to have logic switched: State flags, left = 0, right = 1
        # self.state += ['swing_foot']
        #=======================================================================
        # State flags, left = 0, right = 1
        # ======================================================================
        # self.sample_nb = 6
        # self.sample_max = self.sample_nb

        # Swap left and right leg order, i.e. left position is mapped onto right position etc, including contacts.
        self.inverted_state = ['left_hip_x_pos', 'left_hip_z_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
        self.inverted_state += ['right_hip_x_pos','right_hip_z_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
        self.inverted_state += ['left_hip_x_vel', 'left_hip_z_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']
        self.inverted_state += ['right_hip_x_vel','right_hip_z_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
        self.inverted_state += ['com_z']
        self.inverted_state += ['vx','vz','pitch','pitch_vel']

        # Needs negatives when inverting:
        self.inverted_state += ['vy','roll','yaw']
        self.inverted_state += ['roll_vel','yaw_vel']

        self.inverted_state += ['left1','left2','left']
        self.inverted_state += ['right1','right2','right']
        self.inverted_state += ['prev_left_foot_left_ground','left_foot_left_ground']
        self.inverted_state += ['prev_right_foot_left_ground','right_foot_left_ground']

        self.saved_state = []

    def reset(self, door_data=None):
        # print(self.prev_speed, self.speed)
        self.episodes += 1
        self.ep_lens.append(self.steps)
        self.cur_foot = 'right'
        self.left_on_box = 0.0
        self.right_on_box = 0.0

        if (self.up_stairs or self.down_stairs or self.mh) and self.mode in ['up', 'up_flat', 'down', 'down_flat']:
            # if not self.cur and (self.steps*self.timeStep*2) > 4 and self.steps != 0 and np.mean(self.vel_buffer) > 1.25:
            # if not self.cur and (self.steps*self.timeStep*2) > 4 and self.steps != 0 and np.mean(self.episode_speeds) >= 1.0:
            if not self.cur and len(self.ep_lens) == 5 and (np.mean(self.ep_lens)*self.timeStep*2) > 4 and self.steps != 0 and np.mean(self.episode_speeds) >= 0.9:
                self.ep_lens = deque(maxlen=5)
                # if self.height_coeff < 0.1:
                if self.height_coeff < 0.08:
                    self.height_coeff += 0.01
        if self.cur:
            # if (self.steps*self.timeStep*2) > 3 and self.steps != 0 and np.mean(self.vel_buffer) > 1.25:
            # if (self.steps*self.timeStep*2) > 3 and self.steps != 0:
            if len(self.ep_lens) == 5 and (np.mean(self.ep_lens)*self.timeStep*2) > 3 and self.steps != 0:
                self.ep_lens = deque(maxlen=5)
                self.Kp = self.Kd = self.Kd_x = self.Kp*0.75
                if self.Kp < 10:
                    self.Kd_x = self.Kp = self.Kd = 0
                    self.cur = False
        self.episode_speeds = []
        if self.speed_cur:
            self.speed = np.random.uniform(low=self.min_v,high=self.max_v)
            if self.speed < 0.4: self.speed = 0
            # if self.up_stairs:
            #     self.speed = np.random.uniform(low=0.5,high=3.0)
            # elif self.down_stairs:
            #     self.speed = np.random.uniform(low=0.5,high=4.0)
            # else:
            #     self.speed = np.random.uniform(low=0.5,high=5.0)
            self.episode_speeds.append(self.speed)
            
            self.yaw_v = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
            self.yaw_time = int(abs(200/self.yaw_v))
            self.yaw_count = 0
            self.yawing = True

        if self.speed >= 0.4:
            self.time_of_step = int(60/self.speed)
        else:
            self.time_of_step = 0
        self.vel_buffer = deque(maxlen=200)
        self.action_store = deque(maxlen=self.time_of_step)

        if self.record_step:
            if self.best_reward < self.total_reward and self.total_reward != 0:
                self.best_reward = self.total_reward
                self.save_sim_data(best=True)
                self.save_sim_data()
            else:
                self.save_sim_data(best=False)
            self.sim_data = []
        # ======================================================================
        if self.variable_start and self.episodes % 2 == 0 and self.episodes != 0:
            self.restore()
        else:
            p.resetSimulation()
            self.load_model()
        
        if self.colour:
            self.obstacles.set_colour(self.box_info[0][self.box_num], self.speed)
            self.obstacles.set_colour(self.box_info[0][self.box_num + 1], self.speed)
        self.steps = 0
        self.total_reward = 0
        self.prev_actions = np.zeros(12)
        self.get_observation()
        if self.control:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v]))
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state]))

    def load_model(self):

        if self.MASTER: p.loadMJCF(currentdir + "/ground.xml")
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION + p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        self.Id = objs


        p.setTimeStep(self.timeStep)
        p.setGravity(0,0,-9.8)
        # ======================================================================

        numJoints = p.getNumJoints(self.Id)
        # Camera following robot:
        # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
        # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
        # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

        self.jdict = {}
        self.feet_dict = {}
        self.leg_dict = {}
        # self.feet = ["left1", "left2", "right1", "right2", "left", "right"]
        self.feet = ["left_foot_link", "right_foot_link"]
        self.feet_contact = {f:True for f in self.feet}
        self.ordered_joints = []
        self.ordered_joint_indices = []

        for j in range( p.getNumJoints(self.Id) ):
            info = p.getJointInfo(self.Id, j)
            link_name = info[12].decode("ascii")
            if link_name in self.feet: self.feet_dict[link_name] = j
            self.ordered_joint_indices.append(j)
            if info[2] != p.JOINT_REVOLUTE: continue
            jname = info[1].decode("ascii")
            lower, upper = (info[8], info[9])
            p.enableJointForceTorqueSensor(self.Id, j, True)
            self.ordered_joints.append( (j, lower, upper) )
            self.jdict[jname] = j
        self.motor_names = ["right_hip_x", "right_hip_z", "right_hip_y", "right_knee"]
        self.motor_names += ["left_hip_x", "left_hip_z", "left_hip_y", "left_knee"]
        self.motor_names += ["right_ankle_x", "right_ankle_y"]
        self.motor_names += ["left_ankle_x", "left_ankle_y"]
        self.motor_power = [100, 100, 300, 200]
        self.motor_power += [100, 100, 300, 200]
        self.motor_power += [50, 100, 50, 100]

        self.motors = [self.jdict[n] for n in self.motor_names]
        forces = np.ones(len(self.motors))*240
        self.actions = {key:0.0 for key in self.motor_names}

        # Disable motors to use torque control:
        if self.motor_control == 'torque':
            p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

        # Increase the friction on the feet, we have heaps of friction at our feet.
        for key in self.feet_dict:
        	p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)

        # self.height_coeff = 0.08
        if self.up_down_flat:
            self.box_info = self.obstacles.up_down_flat(height_coeff = self.height_coeff, rand=False, order=self.up_down_flat_arg)
        else:
            if self.mh:
                # self.mode = np.random.choice(['down_flat', 'down', 'up_flat', 'up'], p=[0.25, 0.25, 0.25, 0.25])
                self.mode = np.random.choice(['up_flat', 'up'], p=[0.5, 0.5])
            elif self.up_stairs:
                # self.mode = random.sample(['flat', 'up', 'up_flat'], 1)[0]
                # self.mode = np.random.choice(['flat', 'up', 'up_flat'], p=[0.2,0.4,0.4])
                # self.mode = np.random.choice(['up', 'up_flat'], p=[0.3,0.7])
                self.mode = 'up_flat'
            elif self.down_stairs:
                # self.mode = random.sample(['flat', 'down', 'down_flat'], 1)[0]
                # self.mode = np.random.choice(['flat', 'down', 'down_flat'], p=[0.2,0.4,0.4])
                # self.mode = np.random.choice(['down', 'down_flat'], p=[0.3,0.7])
                self.mode = 'down_flat'
            else:
                self.mode = 'flat'
            # self.length_coeff = 0.2 + (random.random()-0.5)/5
            # self.length_coeff = 0.2 + random.random()/5
            self.length_coeff = 0.18 + random.random()/10
            # self.length_coeff = 0.17
            # self.length_coeff = 0.37
            self.box_info = self.obstacles.add_stairs(length_coeff = self.length_coeff, height_coeff = self.height_coeff, disturb=self.disturbances, speed=self.speed, mode=self.mode)

        self.z_offset = 0
        self.box_num = 0

        self.body_xyz, _ = p.getBasePositionAndOrientation(self.Id)
        # if self.down_stairs or self.up_stairs or self.up_down_flat or self.mh:
        if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
            self.box_num += 1
            # self.right_on_box = 0.0
            # self.left_on_box = 0.0
        if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
            self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        else:
            self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        self.restore()

    def restore(self, sample=None):
        # Restore state every second time (need to always pick a state to restore starting from 0).
        # print(self.variable_start and self.episodes % 2 == 0 and self.episodes != 0 and self.saved_state)
        if self.variable_start and self.episodes % 2 == 0 and self.episodes != 0 and self.saved_state:
            pos, orn, joints, base_vel, joint_vel, saved_dict, others = self.saved_state
            self.ob_dict = saved_dict
            self.box_num, self.cur_foot, self.steps = others
            # [pos, orn, rot, vel, joint, joint_vel]
            # c = [0.01, 0.001, 0.01, 0.01, 0.0, 0.0]
            # c = [0.015, 0.006, 0.012, 0.4, 0.01, 0.01]
            # c = [0.015, 0.006, 0.012, 0.4, 0.25, 0.2]
            c = [0.015, 0.006, 0.012, 0.4, 0.05, 0.05]
            self.saved_state = []
        else:
            self.ob_dict = {'prev_right_foot_left_ground':False,'prev_left_foot_left_ground':False,'left_foot_left_ground':False,'right_foot_left_ground':False}
            self.ob_dict.update({'roll':0.0,'pitch':0.0,'yaw':0.0})
            self.ob_dict['ready_to_walk'] = False
            self.ob_dict['swing_foot'] = 1
            self.cur_foot = 'right'
            # random_box = random.sample([_ for _ in range(10)],1)[0]
            # box_x, box_z = self.box_info[1][random_box][0], self.box_info[1][random_box][2] + self.box_info[2][random_box][2]
            # pos, orn, joints, base_vel, joint_vel = [box_x,0,box_z+1],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            box_z = self.box_info[1][0][2] + self.box_info[2][0][2]
            pos, orn, joints, base_vel, joint_vel = [0,0,box_z+1],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            self.sample = random.sample([_ for _ in range(150)], 1)[0]
            self.save_state = []
            # [pos, orn, rot, vel, joint, joint_vel]
            # c = [0.015, 0.006, 0.012, 0.4, 0.01, 0.01]
            # c = [0.015, 0.006, 0.012, 0.4, 0.15, 0.1]
            c = [0.015, 0.006, 0.012, 0.4, 0.05, 0.05]

        if self.disturbances:
            pos[0] += np.random.uniform(low=-c[0], high=c[0])
            pos[1] += np.random.uniform(low=-c[0], high=c[0])
            pos[2] += np.random.uniform(low=-c[0], high=c[0])
            orn[0] += np.random.uniform(low=-c[1], high=c[1])
            orn[1] += np.random.uniform(low=-c[1], high=c[1])
            orn[2] += np.random.uniform(low=-c[1], high=c[1])
            orn[3] += np.random.uniform(low=-c[1], high=c[1])
            for i in range(len(base_vel[0])):
                base_vel[0][i] += np.random.uniform(low=-c[2], high=c[2])
                base_vel[1][i] += np.random.uniform(low=-c[3], high=c[3])
            for i, j in enumerate(self.ordered_joints):
                joints[i] += np.clip(np.random.uniform(low=-c[4], high=c[4]), j[1], j[2])
                joint_vel[i] += np.random.uniform(low=-c[5], high=c[5])
        self.set_position(pos, orn, joints, base_vel, joint_vel)

    def get_sample(self):
        pos = list(self.body_xyz)
        orn = [self.qx, self.qy, self.qz, self.qw]
        joints = [self.ob_dict[m + '_pos'] for m in self.motor_names]
        joint_vel = [self.ob_dict[m + '_vel'] for m in self.motor_names]
        base_vel = [list(self.body_vxyz), list(self.base_rot_vel)]
        saved_dict = self.ob_dict
        others = [self.box_num, self.cur_foot, self.steps]
        self.saved_state = copy.deepcopy([pos, orn, joints, base_vel, joint_vel, saved_dict, others])
        return self.saved_state

    def seed(self, seed=None):
    	self.np_random, seed = seeding.np_random(seed)
    	return [seed]

    def close(self):
    	print("closing")

    def step(self, actions):
        if self.speed_cur and (self.steps*self.timeStep) % 3 == 0 and self.steps != 0:
            self.speed = np.random.uniform(low=self.min_v,high=self.max_v)           
            if self.speed < 0.4: self.speed = 0
            # if self.up_stairs:
            #     self.speed = np.random.uniform(low=0.5,high=3.0)
            # elif self.down_stairs:
            #     self.speed = np.random.uniform(low=0.5,high=4.0)
            # else:
            #     self.speed = np.random.uniform(low=0.5,high=5.0)
            self.episode_speeds.append(self.speed)
        
            self.yaw_v = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
            self.yaw_time = int(abs(200/self.yaw_v))
            self.yaw_count = 0
            self.yawing = True
        
            if self.colour:
                for box in range(self.box_num, len(self.box_info[0])):
                    self.obstacles.set_colour(self.box_info[0][box], self.speed)
            if self.speed >= 0.4:
                self.time_of_step = int(60/self.speed)
            else:
                self.time_of_step = 0
            # self.time_of_step = int(60/self.speed)
            # self.vel_buffer = deque(maxlen=self.time_of_step)
            self.action_store = deque(maxlen=self.time_of_step)
        # if self.speed_cur:
        #     if (self.steps*self.timeStep) % 2 == 0 and self.steps != 0 and not self.yawing and self.speed > 0:
                
        if self.yawing:
            if self.yaw_count < self.yaw_time:
                self.yaw_count += 1
                if self.yaw_count == int(self.yaw_time/4) or self.yaw_count == int(3*self.yaw_time/4):
                    self.yaw_v *= -1
            else:
                self.yawing = False
                self.yaw_v = 0

        # if self.steps > 150 and self.disturbances and random.random() < 0.02:
        if self.disturbances and random.random() < 0.02:
            self.add_disturbance()
        t1 = time.time()
        actions = np.clip(actions, -10, 10)
        if self.cur:
            actions = self.apply_forces(actions)
            # self.apply_forces(actions)
        for _ in range(self.actionRepeat):
            if self.motor_control == 'torque':
                forces = [0.] * len(self.motor_names)
                for m in range(len(self.motor_names)):
                	forces[m] = self.motor_power[m]*actions[m]*0.082
                    # self.apply_forces()
                p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)
            elif self.motor_control == 'position':
                p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=actions, forces=self.motor_power)
            if self.MASTER:
                p.stepSimulation()
        self.get_observation()
        self.vel_buffer.append(self.ob_dict['vx'])
        # if self.down_stairs or self.up_stairs or self.up_down_flat or self.mh:
        if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
            self.box_num += 1
            # self.right_on_box = 0.0
            # self.left_on_box = 0.0
        if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
            self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        else:
            self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]

        reward, done = self.reward(actions)
        self.total_reward += reward
        if self.record_step: self.record_sim_data()
        if self.variable_start and self.steps == self.sample and not done:
            self.get_sample()
        self.steps += 1
        self.total_steps += 1
        if self.control:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v])), reward, done, self.ob_dict
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state])), reward, done, self.ob_dict

    def reward(self, actions):
        done = False
        x = abs(self.ob_dict['vx'] - self.speed)
        # reward = (1/(np.e**x + np.e**-x))*(1.5/0.5)
        # reward = (3/(np.e**(4*x) + np.e**(-4*x)))
        reward = 0.75*(3/(np.e**(3*x) + np.e**(-3*x)))

        x = abs(self.ob_dict['yaw_vel'] - self.yaw_v)
        reward += 0.25*(3/(np.e**(3*x) + np.e**(-3*x)))

        # reward = min(self.ob_dict['vx'], self.speed)
        reward -= 0.005*np.square(actions).sum()
        # if self.steps > 1000: done = True
        if self.steps > 2000: done = True
        # if (self.body_xyz[2] < (0.75 + self.z_offset) or abs(self.body_xyz[1]) > 0.3 ):
        if (self.body_xyz[2] < (0.75 + self.z_offset) or abs(self.body_xyz[1]) > 0.75 ):
            done = True
        # Keep com steady/no anglular displacement
        reward -= (0.5*abs(self.ob_dict['pitch']) + 0.5*abs(self.ob_dict['roll']) + 0.5*abs(self.ob_dict['yaw']))
        # Stay in middle
        # if abs(self.body_xyz[1]) > 0.1:
        #     # reward -= self.body_xyz[1]**2
        #     reward -= abs(self.body_xyz[1])
        # No twisting of feet
        reward -= 0.5*(self.ob_dict['left_hip_z_pos']**2 + self.ob_dict['right_hip_z_pos']**2)
        # Ensure no jumping/running
        if self.speed <= 1.0 and self.ob_dict['left_foot_left_ground'] and self.ob_dict['right_foot_left_ground'] :
            reward -= 0.5
        left_foot = p.getLinkState(self.Id, self.feet_dict["left_foot_link"])[0]
        right_foot = p.getLinkState(self.Id, self.feet_dict["right_foot_link"])[0]
        # Make feet suitable width apart
        if (left_foot[1] - 0.02) < (right_foot[1] + 0.02):
            reward -= 0.2
        if not self.ob_dict['left_foot_left_ground'] and self.ob_dict['left_knee_pos'] < -0.15 and abs(left_foot[0] - right_foot[0]) < 0.2:
            reward -= 0.05*(self.ob_dict['left_knee_pos'])**2
            # print(0.05*(self.ob_dict['left_knee_pos'])**2, self.ob_dict['left_knee_pos'])
        if not self.ob_dict['right_foot_left_ground'] and self.ob_dict['right_knee_pos'] < -0.15 and abs(left_foot[0] - right_foot[0]) < 0.2:
            reward -= 0.05*(self.ob_dict['right_knee_pos'])**2
            # print(0.05*(self.ob_dict['right_knee_pos'])**2, self.ob_dict['right_knee_pos'], self.ob_dict['right_hip_y_pos'])

        #     reward -= 0.008*(self.ob_dict['com_z'] - 1)**2
        # Constant step length
        # if self.speed >= 1.0 and self.box_num > 0:
        #     if not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground'] and (abs(left_foot[0] - right_foot[0]) < self.step_length*2 - 0.15):
        #         reward -= (abs(left_foot[0] - right_foot[0]) - self.step_length*2)**2
        #     prev_box_x = self.box_info[1][self.box_num-1][0]
        #     box_x = self.box_info[1][self.box_num][0]
        #     next_box_x = self.box_info[1][self.box_num+1][0]
        #     if not self.ob_dict['right_foot_left_ground']:
        #         reward -= min((right_foot[0] - prev_box_x)**2, (right_foot[0] - box_x)**2,(right_foot[0] - next_box_x)**2)
        #         # print("right", 5*min((right_foot[0] - prev_box_x)**2, (right_foot[0] - box_x)**2,(right_foot[0] - next_box_x)**2))
        #     elif not self.ob_dict['left_foot_left_ground']:
        #         reward -= min((left_foot[0] - prev_box_x)**2, (left_foot[0] - box_x)**2, (left_foot[0] - next_box_x)**2)
        #         # print("left", 5*min((left_foot[0] - prev_box_x)**2, (left_foot[0] - box_x)**2, (left_foot[0] - next_box_x)**2))
            # step_ratio = (self.steps % self.time_of_step) / self.time_of_step
            # if (step_ratio > 0.9 or step_ratio < 0.1):
            #     if self.speed >= 1:
            #         reward -= (abs(left_foot[0] - right_foot[0]) - self.step_length*2)**2
            #     elif abs(left_foot[0] - right_foot[0]) < 0.2:
            #         reward -= (abs(left_foot[0] - right_foot[0]) - 0.2)**2
        # Straight knee when in stance
        # if not self.ob_dict['left_foot_left_ground'] and abs(left_foot[0] - self.body_xyz[0]) < 0.1:
        #     reward -= 0.05*(self.ob_dict['left_knee_pos'] - 0.1)**2
        #     reward -= 0.008*(self.ob_dict['com_z'] - 1)**2
        # elif not self.ob_dict['right_foot_left_ground'] and abs(right_foot[0] - self.body_xyz[0]) < 0.1:
        #     reward -= 0.05*(self.ob_dict['right_knee_pos'] - 0.1)**2
        #     reward -= 0.008*(self.ob_dict['com_z'] - 1)**2
        # Symmetry penalty
        # if self.mode in ['flat', 'up', 'down']:
        if self.time_of_step != 0 and len(self.action_store) == (self.time_of_step):
            prev_z = z = next_z = 0
            if self.box_info is not None and (self.box_num+1) < len(self.box_info[0]) and self.box_num > 0:
                prev_z = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
                z = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
                next_z = self.box_info[1][self.box_num+1][2] + self.box_info[2][self.box_num+1][2]
            if round(z - prev_z,2) == round(next_z - z,2):
                reward -= 0.008*np.sum((actions[:4] - self.action_store[0][4:8])**2)
                reward -= 0.008*np.sum((actions[8:10] - self.action_store[0][10:12])**2)
                # print(0.005*np.sum((actions[:4] - self.action_store[0][4:8])**2) + 0.005*np.sum((actions[8:10] - self.action_store[0][10:12])**2))
        self.action_store.append(actions)
        # Don't step over a box, don't step twice on the same box, step close to box centre. Only if going fast enough to care.
        # print(self.box_num, self.speed)
        # Ensure step length consistent, step_length = 0.3, we want it to be 0.6

        # if self.box_num > 0 and self.speed >= 1.0 and not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']:
            # reward -= 15*(abs(left_foot[0] - right_foot[0]) - self.step_length*2)**2
            # print(left_foot[0],right_foot[0])
            # print(15*(abs(left_foot[0] - right_foot[0]) - self.step_length*2)**2)



            # print((abs(left_foot[0] - right_foot[0]) - self.step_length*2)**2)
        # Only if steps are visible
        # if self.height_coeff > 0 and self.box_num > 0 and len(self.box_info[0]) > self.box_num + 1 and self.speed >= 1.0:
        # if self.box_num > 0 and len(self.box_info[0]) > self.box_num + 1 and self.speed >= 1.0:
        #     # Step close to box centre
        #     # prev_box_x = self.box_info[1][self.box_num-1][0]
        #     box_x = self.box_info[1][self.box_num][0]
        #     # next_box_x = self.box_info[1][self.box_num+1][0]
        #     # if not self.ob_dict['right_foot_left_ground']:
        #     #     reward -= 5*min((right_foot[0] - prev_box_x)**2, (right_foot[0] - box_x)**2,(right_foot[0] - next_box_x)**2)
        #     # elif not self.ob_dict['left_foot_left_ground']:
        #     #     reward -= 5*min((right_foot[0] - prev_box_x)**2, (left_foot[0] - box_x)**2, (left_foot[0] - next_box_x)**2)
        #     # If at any stage the step is too big (skips a box) then penalize
        #     # if (left_foot[0] < x1 and right_foot[0] > x2) or (right_foot[0] < x1 and left_foot[0] > x2):
        #     #     reward -= 0.5
        #     # Whats the x position that each foot is in contact with the ground
        #     if (self.ob_dict['prev_left_foot_left_ground'] and not self.ob_dict['left_foot_left_ground']):
        #         self.left_on_box = left_foot[0]
        #     if (self.ob_dict['prev_right_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']):
        #         self.right_on_box = right_foot[0]
        #     # If both feet contact inside same box, penalize
        #         # if (self.left_on_box < prev_box_x + self.step_length and self.left_on_box > prev_box_x - self.step_length) and (self.right_on_box < prev_box_x + self.step_length and self.right_on_box > prev_box_x - self.step_length):
        #         #     reward -= 0.2
        #             # print("prev", abs(self.right_on_box - self.left_on_box), self.left_on_box, self.right_on_box, prev_box_x - self.step_length, prev_box_x + self.step_length)
        #     if (self.left_on_box > 0.0 and self.right_on_box > 0.0) and (self.left_on_box < box_x + self.step_length and self.left_on_box > box_x - self.step_length) and (self.right_on_box < box_x + self.step_length and self.right_on_box > box_x - self.step_length):
        #         reward -= 0.8
                # print("current", abs(self.right_on_box - self.left_on_box), self.left_on_box, self.right_on_box, box_x - self.step_length, box_x + self.step_length)
                # if (self.left_on_box < next_box_x + self.step_length and self.left_on_box > next_box_x - self.step_length) and (self.right_on_box < next_box_x + self.step_length and self.right_on_box > next_box_x - self.step_length):
                #     reward -= 0.2
                #     print("next", self.left_on_box, self.right_on_box, next_box_x - self.step_length, next_box_x + self.step_length)
                # done = True
        # if self.steps > 5000: done = True
        return reward, done

    def get_observation(self):
        # Get joint positions and velocities
        jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
        # ======================================================================
        self.ob_dict.update({self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(self.ac_size)])})
        self.ob_dict.update({self.state[i+self.ac_size]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(self.ac_size)])})

        # Get relative com
        self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
        # Get angles and velocities
        roll, pitch, yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])
        prev_r, prev_p, prev_y = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
        self.ob_dict.update({'roll':roll,'pitch':pitch,'yaw':yaw})
        self.ob_dict.update({'roll_vel':self.base_rot_vel[0],'pitch_vel':self.base_rot_vel[1],'yaw_vel':self.base_rot_vel[2]})
        # print('yaw_vel', self.base_rot_vel[2])
        rot_speed = np.array(
        	[[np.cos(-yaw), -np.sin(-yaw), 0],
        	 [np.sin(-yaw), np.cos(-yaw), 0],
        	 [		0,			 0, 1]]
        )
        vx, vy, vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))  # rotate speed back to body point of view
        self.ob_dict.update({'vx':vx,'vy':vy,'vz':vz})
        # Get feet contacts
        self.ob_dict['prev_right_foot_left_ground'] = self.ob_dict['right_foot_left_ground']
        self.ob_dict['prev_left_foot_left_ground'] = self.ob_dict['left_foot_left_ground']

        self.ob_dict['right_foot_on_ground']= len(p.getContactPoints(self.Id, -1, self.feet_dict['right_foot_link'], -1))>0
        self.ob_dict['left_foot_on_ground']= len(p.getContactPoints(self.Id, -1, self.feet_dict['left_foot_link'], -1))>0

        if self.ob_dict['swing_foot']: stance = 'left'; swing = 'right'
        else: stance = 'right'; swing = 'left'

        if not self.ob_dict['ready_to_walk'] and (not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']):
            self.ob_dict['ready_to_walk'] = True
            self.ob_dict['prev_left_foot_left_ground'] = False
            self.ob_dict['prev_right_foot_left_ground'] = False

        # Check if we should change swing and stance feet.
        if self.ob_dict['ready_to_walk'] and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
            self.ob_dict['swing_foot'] = not self.ob_dict['swing_foot']
            self.ob_dict['impact'] = True
        else: self.ob_dict['impact'] = False

        self.ob_dict.update({'com_z': self.body_xyz[2] - self.z_offset})

    def update_speed(self, speed):
        self.speed = speed
        return np.array([self.ob_dict[s] for s in self.state] + [self.speed])

    def update_speed_and_opt(self, speed, opt_buffer):
        self.speed = speed
        # option = [0]*num_options
        # option[opt] = 1
        return np.array([self.ob_dict[s] for s in self.state] + [self.speed] + list(opt_buffer))

    def dict_from_obs(self, obs):
        # print(obs.shape)
        return {s:o for s, o in zip(self.state, obs)}

    def add_disturbance(self):
        # max_disturbance = 250
        # max_disturbance = 1000
        # max_disturbance = 500
        max_disturbance = 1000
        force_x = (random.random() - 0.5)*max_disturbance
        force_y = (random.random() - 0.5)*max_disturbance
        force_z = (random.random() - 0.5)*(max_disturbance/4)
        # print("applying forces", force_x, force_y, force_z)
        p.applyExternalForce(self.Id,-1,[force_x,force_y,0],[0,0,0],p.LINK_FRAME)

    def apply_forces(self, actions=None):

        roll, pitch, yaw = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        vx, vy, vz = self.ob_dict['vx'], self.ob_dict['vy'], self.ob_dict['vz']
        y_force = - (0.1 * self.Kp * self.body_xyz[1] + self.Kd * vy)
        x_force = self.Kd_x * (self.speed - vx)
        if self.motor_control == 'torque':
            z_force = 1.5*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        else:
            z_force = 0
        p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)

        pitch_force = -self.Kp * pitch
        # yaw_force = -self.Kp * yaw
        yaw_force = -self.Kd * self.ob_dict['yaw_vel'] * np.pi/180
        roll_force = -self.Kp * roll
        p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)

        if self.speed > 0.0:
            hip_torque = 0
            stance_hip_torque = 0
            knee_torque = 0
            stance_knee_torque = 0
            if self.cur_foot == 'left':
                stance = 'right'
                hip_num = 6
                knee_num = 7
                stance_hip_num = 2
                stance_knee_num = 3
            else:
                stance = 'left'
                hip_num = 2
                knee_num = 3
                stance_hip_num = 6
                stance_knee_num = 7
            # print(self.speed, self.time_of_step)
            cur_time = self.steps % self.time_of_step
            dx = 0.12
            if self.ob_dict[self.cur_foot + '_hip_y_pos'] > -0.5:
                hip_torque = 10*(self.Kp/400)*(-dx)
            if self.ob_dict[stance + '_hip_y_pos'] < 0.25:
                stance_hip_torque = 2*(self.Kp/400)*(dx)
            if self.ob_dict[self.cur_foot + '_knee_pos'] > -1.0 and cur_time < 10:
                knee_torque = 4 *(self.Kp/400)*(-dx)
            if self.ob_dict[self.cur_foot + '_knee_pos'] < -0.5 and cur_time > 10:
                knee_torque = 4*(self.Kp/400)*(dx)
            if self.ob_dict[stance + '_knee_pos'] < -0.5:
               stance_knee_torque = 8  *(self.Kp/400)*(dx)
            actions[knee_num] += knee_torque
            actions[hip_num] += hip_torque
            actions[stance_knee_num] += stance_knee_torque
            actions[stance_hip_num] += stance_hip_torque
            if self.steps != 0 and self.steps % self.time_of_step == 0:
                if self.cur_foot == 'left': self.cur_foot = 'right'
                else: self.cur_foot = 'left'
        return actions

    def save_sim_data(self, best=False, worst=False, trans=False):
        comm = MPI.COMM_WORLD
        if comm.Get_rank() == 0:
            if trans: path = self.PATH + 'trans_'
            elif best: path = self.PATH + 'best_'
            elif worst: path = self.PATH + 'worst_'
            else: path = self.PATH
            try:
                np.save(path + 'sim_data.npy', np.array(self.sim_data))
                if path != self.PATH:
                    np.save(self.PATH + 'sim_data.npy', np.array(self.sim_data))
                if (self.up_stairs or self.down_stairs or self.up_down_flat or self.mh) and self.box_info is not None:
                    np.save(path + 'box_info_pos.npy', np.array(self.box_info[1]))
                    np.save(path + 'box_info_size.npy', np.array(self.box_info[2]))
                    np.save(path + 'box_info_frict.npy', np.array(self.box_info[3]))
                    if path != self.PATH:
                        np.save(self.PATH + 'box_info_pos.npy', np.array(self.box_info[1]))
                        np.save(self.PATH + 'box_info_size.npy', np.array(self.box_info[2]))
                        np.save(self.PATH + 'box_info_frict.npy', np.array(self.box_info[3]))
            except Exception as e:
                print("Save sim data error:")
                print(e)


    def record_sim_data(self):

    	if len(self.sim_data) > 100000: return
    	data = [self.body_xyz, [self.qx, self.qy, self.qz, self.qw]]
    	joints = p.getJointStates(self.Id, self.motors)
    	data.append([i[0] for i in joints])
    	self.sim_data.append(data)

    def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None):
        pos = [pos[0], pos[1]+self.y_offset, pos[2]]
        p.resetBasePositionAndOrientation(self.Id, pos, orn)
        if joints is not None:
            if joint_vel is not None:
                for j, jv, m in zip(joints, joint_vel, self.motors):
                    p.resetJointState(self.Id, m, targetValue=j, targetVelocity=jv)
            else:
                for j, m in zip(joints, self.motors):
                	p.resetJointState(self.Id, m, targetValue=j)
        if velocities is not None:
            p.resetBaseVelocity(self.Id, velocities[0], velocities[1])

    def transform_no_rot(self, pos, point):
        trans = np.array([[1,0,0,pos[0]],[0,1,0,pos[1]],[0,0,1,pos[2]],[0,0,0,1]])
        return np.dot(trans,[point[0],point[1],point[2], 1])

    def get_image(self):
        camTargetPos = [0.,0.,0.]
        cameraUp = [0,0,1]
        cameraPos = [1,1,1]
        yaw = 40
        pitch = 10.0

        roll=0
        upAxisIndex = 2
        camDistance = 4
        # new ===================
        pixelWidth = 48
        pixelHeight = 48
        cam_dz = -0.0
        dx = 0.5
        # nearPlane = 0.01
        # farPlane = 1000
        lightDirection = [0,1,0]
        lightColor = [1,1,1]#optional argument
        # fov = 60
        pos, orn = p.getBasePositionAndOrientation(self.Id)
        roll, pitch, yaw = p.getEulerFromQuaternion(orn)
        dy = 0
        dz = -1
        cam_dx = 0.2
        cam_dy = 0

        p_x, p_y, p_z, _ = self.transform_no_rot(pos, [dx,dy,dz])
        cam_x, cam_y, cam_z, _ = self.transform_no_rot(pos, [cam_dx, cam_dy, cam_dz])
        viewMatrix = p.computeViewMatrix([cam_x,cam_y,cam_z], [p_x, p_y, p_z], cameraUp)
        try:
            image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix, projectionMatrix=self.projectionMatrix, lightDirection=lightDirection,lightColor=lightColor, renderer = p.ER_TINY_RENDERER)
            # image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix=viewMatrix, renderer = p.ER_TINY_RENDERER)
            # rgb = image[2]/255.0
            rgb = np.array(image[2]).reshape(pixelWidth, pixelHeight,4)/255.0
            # rgb = image[2][:,:,:3]/255.0
            depth = np.array(image[3]).reshape(pixelWidth,pixelHeight,1)

            # if down_stairs, block off 75% of the image. It can see too far, so if any changes far in the image, the policy is affected
            if self.down_stairs:
                if random.random() < 0.5:
                    rgb[:int(48*0.4),:,:] = 0
                    depth[:int(48*0.4),:] = 0
            # rgb[:,:,:] = 0
            # depth[:,:,:] = 0
            # re_im = rgb*255.0
            # cv2.imshow('robot_vision',re_im.astype(dtype=np.uint8))
            # cv2.waitKey(1)
            self.im = np.nan_to_num(np.concatenate((rgb,depth), axis=2))
            return self.im

        except Exception as e:
            print(e)
            return np.zeros([pixelWidth, pixelHeight, 4])

    def invert(self):
        inverted_ob = []
        for s in self.inverted_state:
            if s in ['vy','roll','yaw','roll_vel','yaw_vel']:
                inverted_ob.append(-self.ob_dict[s])
            else:
                inverted_ob.append(self.ob_dict[s])
        inverted_ob = np.array(inverted_ob + [self.speed])
        inverted_im = np.flip(self.im, 1)
        # im = (self.im[:,:,:3]*255.0).astype(dtype=np.uint8)
        # inv_im = (inverted_im[:,:,:3]*255.0).astype(dtype=np.uint8)
        # concat_im = np.hstack([im, inv_im])
        # cv2.imshow('robot_vision',concat_im)
        # cv2.waitKey(1)
        return inverted_ob, inverted_im


if __name__ == "__main__":
    from mpi4py import MPI
    import argparse
    import cv2
    parser = argparse.ArgumentParser()
    parser.add_argument('--render',default=False,action='store_true')
    parser.add_argument('--cur',default=False,action='store_true')
    parser.add_argument('--up_stairs',default=False,action='store_true')
    parser.add_argument('--down_stairs',default=False,action='store_true')
    args = parser.parse_args()
    env = Env()

    PATH = '/home/brendan/results/thing/'
    env.arguments(RENDER=args.render, PATH=PATH, cur=args.cur, record_step=False, up_stairs=args.up_stairs, down_stairs=args.down_stairs)
    env.height_coeff = 0.1

    ob = env.reset()
    i = 0
    while True:
        # print(boxes)
        actions = (np.random.rand(12)-0.5)*2

        ob, reward, done, ob_dict = env.step(actions)
        env.get_image()
        time.sleep(0.01)
        # cv2.imshow("frame", cv2.cvtColor(image[2], cv2.COLOR_BGR2RGB))
        # cv2.imshow("frame", image)
        # cv2.waitKey(1)
        if done:
            # print(done)
            env.reset()
            # env.obstacles.create_rand_floor()
        i += 1
