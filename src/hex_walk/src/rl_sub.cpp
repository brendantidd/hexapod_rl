#include "ros/ros.h"
#include "std_msgs/String.h"

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("I heard: [%s]", msg->data.c_str());

}


// void jsCallback(const std_msgs::String::ConstPtr& msg)
// void jsCallback(const DSH1_msgs::String::ConstPtr& msg)
// {
//     ROS_INFO("I heard: [%s]", msg->data.c_str());

// }

int main(int argc, char **argv)
{
    ros::init(argc, argv, "listener");

    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);

    // Need to subscribe to joint states to get all joint pos and vel
    // ros::Subscriber sub = n.subscribe("DSH1/joint_states", 1000, jsCallback);

    ros::spin();

    return 0;

}