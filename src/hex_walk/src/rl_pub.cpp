// ************************************************************
// Get observation, run forward policy, publish torque commands
// ************************************************************
#include "ros/ros.h"
#include "std_msgs/String.h"
// #include "geometry_msgs/Twist.h"
#include "geometry_msgs/TwistStamped.h"
// #include "std_srvs/"
#include <sstream>
#include <std_srvs/SetBool.h>
#include <std_srvs/Trigger.h>

bool manual_velocity_override_ = false;


bool manualVelocityOverrideCallback(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& res)
{
  manual_velocity_override_ = req.data;
  bool successfully_set_override_request = (manual_velocity_override_ == req.data);
  res.success = successfully_set_override_request;
  if (successfully_set_override_request)
  {
    if (manual_velocity_override_)
    {
      res.message = "Manual body velocity override successful ENABLED, ";
      res.message += "body velocity is now commanded manually from user joystick input.";
    }
    else
    {
      res.message = "Manual body velocity override successfully DISABLED, ";
      res.message += "body velocity is now commanded autonomously from local path planner.";
    }
  }
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "talker");

    ros::NodeHandle n;

    // ros::ServiceServer  = n.advertiseService("locomotion_engine/manual_velocity_override", 1);
    ros::ServiceServer manual_override_service_server_ = n.advertiseService("locomotion_engine/manual_velocity_override", manualVelocityOverrideCallback);

    //                                                     &LocomotionEngineNode::manualVelocityOverrideCallback, this);

    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    // ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("vel_pub", 1000);
    ros::Publisher vel_pub = n.advertise<geometry_msgs::TwistStamped>("r1/locomotion_engine/manual_velocity_command", 1);



    // ros::Publisher vel_pub_stamped = nh.advertise<geometry_msgs::TwistStamped>('locomotion_engine/manual_velocity_command', 1);
    // ros::Publisher vel_pub = nh.advertise<geometry_msgs::Twist>('locomotion_engine/manual_velocity_command', 1);

    ros::Rate loop_rate(10);

    int count = 0;
    while (ros::ok())
    {
        std_msgs::String msg;
        
        std::stringstream ss;
        ss << "hello world" << count;
        msg.data = ss.str();
        chatter_pub.publish(msg);
        
        // geometry_msgs::Twist twist_msg;
        geometry_msgs::TwistStamped stamped_msg;


        // manual_velocity_publisher_.publish(msg);
        stamped_msg.header.stamp = ros::Time::now();
        stamped_msg.twist.linear.x = 1.0;
        
        // msg.linear.y = deadman_active_ ? joy_msg_.axes
        // msg.angular.z = deadman_active_ ? joy_msg_.axes[config_.angular_velocity_axis] : 0.0;

        // ROS_INFO("%s", msg.data.c_str());
        
        // stamped_msg.header.stamp = ros::Time::now();
        // stamped_msg.twist = msg;
        vel_pub.publish(stamped_msg);
        ROS_INFO("%d", stamped_msg.twist.linear.x);


        ros::spinOnce();

        loop_rate.sleep();

        ++count;

    }

    return 0;
}
