Mass properties of ASM_Tibia
     Configuration: Default
     Coordinate system: URDF_COORD-Tibia

Mass = 394.35 grams

Volume = 277982.88 cubic millimeters

Surface area = 195814.83  square millimeters

Center of mass: ( millimeters )
	X = 63.27
	Y = -1.05
	Z = 11.06

Principal axes of inertia and principal moments of inertia: ( grams *  square millimeters )
Taken at the center of mass.
	 Ix = ( 1.00, -0.05, -0.02)   	Px = 1435214.97
	 Iy = ( 0.04,  0.54,  0.84)   	Py = 5801792.48
	 Iz = (-0.03, -0.84,  0.54)   	Pz = 6112775.95

Moments of inertia: ( grams *  square millimeters )
Taken at the center of mass and aligned with the output coordinate system.
	Lxx = 1449475.61	Lxy = -241381.57	Lxz = -74603.99
	Lyx = -241381.57	Lyy = 6008865.49	Lyz = 145721.96
	Lzx = -74603.99	Lzy = 145721.96	Lzz = 5891442.31

Moments of inertia: ( grams *  square millimeters )
Taken at the output coordinate system.
	Ixx = 1498123.22	Ixy = -267550.19	Ixz = 201287.91
	Iyx = -267550.19	Iyy = 7635802.95	Iyz = 141148.83
	Izx = 201287.91	Izy = 141148.83	Izz = 7470599.68

Moments of inertia: ( grams *  square millimeters )
Taken at the output coordinate system.
	Ixx = 0.001498123	Ixy = -0.00026755	Ixz = 0.000201288
	Iyx = -0.00026755	Iyy = 0.007635803	Iyz = 0.000141149
	Izx = 0.000201288	Izy = 0.000141149	Izz = 0.0074706