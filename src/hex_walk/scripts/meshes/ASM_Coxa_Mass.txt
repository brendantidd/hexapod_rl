Mass properties of ASM_Coxa
     Configuration: Default
     Coordinate system: URDF_COORD_Coxa

Mass = 1366.12 grams

Volume = 610394.00 cubic millimeters

Surface area = 225642.90  square millimeters

Center of mass: ( millimeters )
	X = 24.75
	Y = -0.20
	Z = 59.42

Principal axes of inertia and principal moments of inertia: ( grams *  square millimeters )
Taken at the center of mass.
	 Ix = ( 0.14, -0.01,  0.99)   	Px = 1085277.00
	 Iy = ( 0.98, -0.13, -0.14)   	Py = 5625775.71
	 Iz = ( 0.13,  0.99, -0.01)   	Pz = 5725686.37

Moments of inertia: ( grams *  square millimeters )
Taken at the center of mass and aligned with the output coordinate system.
	Lxx = 5539889.77	Lxy = -19672.42	Lxz = 624728.21
	Lyx = -19672.42	Lyy = 5723441.40	Lyz = -46216.54
	Lzx = 624728.21	Lzy = -46216.54	Lzz = 1173407.90

Moments of inertia: ( grams *  square millimeters )
Taken at the output coordinate system.
	Ixx = 10363145.32	Ixy = -26339.66	Ixz = 2634132.13
	Iyx = -26339.66	Iyy = 11383785.59	Iyz = -62220.03
	Izx = 2634132.13	Izy = -62220.03	Izz = 2010602.75

Moments of inertia: ( Kg *  square meter )
Taken at the output coordinate system.
	Ixx = 0.010363145	Ixy = -0.00002634	Ixz = 0.002634132
	Iyx = -0.00002634	Iyy = 0.011383786	Iyz = -0.00006222
	Izx = 0.002634132	Izy = -0.00006222	Izz = 0.002010603