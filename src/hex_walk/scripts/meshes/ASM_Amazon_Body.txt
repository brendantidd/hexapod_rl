Mass properties of ASM_Amazon_Body
     Configuration: Default
     Coordinate system: Coordinate System1

Mass (user-overridden) = 8500.00000 grams
Mass (user-overridden) = 8.50000 kilograms

Volume = 1206969.00357 cubic millimeters

Surface area = 1064944.14759  square millimeters

Center of mass: ( millimeters )
	X = 7.52927
	Y = 0.00000
	Z = 79.27665

Principal axes of inertia and principal moments of inertia: ( grams *  square millimeters )
Taken at the center of mass.
	 Ix = ( 0.98596, -0.16509, -0.02495)   	Px = 132750863.58845
	 Iy = (-0.01354,  0.06989, -0.99746)   	Py = 333337879.09513
	 Iz = ( 0.16642,  0.98380,  0.06668)   	Pz = 345931745.62908

Moments of inertia: ( grams *  square millimeters )
Taken at the center of mass and aligned with the output coordinate system.
	Lxx = 138691509.51470	Lxy = -34712195.20992	Lxz = -5073615.78082
	Lyx = -34712195.20992	Lyy = 340059946.23972	Lyz = -0.87490
	Lzx = -5073615.78082	Lzy = -0.87490	Lzz = 333269032.55824

Moments of inertia: ( grams *  square millimeters )
Taken at the output coordinate system.
	Ixx = 192112207.46440	Ixy = -34712195.17813	Ixz = -4.89103
	Iyx = -34712195.17813	Iyy = 393962508.49388	Iyz = -0.54018
	Izx = -4.89103	Izy = -0.54018	Izz = 333750896.86269

Moments of inertia: ( kilograms * square meters )
Taken at the output coordinate system.
	Ixx = 0.19211	Ixy = -0.03471	Ixz = 0.00000
	Iyx = -0.03471	Iyy = 0.39396	Iyz = 0.00000
	Izx = 0.00000	Izy = 0.00000	Izz = 0.33375