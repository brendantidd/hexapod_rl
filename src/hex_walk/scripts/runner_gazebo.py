#!/usr/bin/env python
'''
Bridge between ros and pybullet/RL libraries so that we can use lego walking gaits with the hexapod in pybullet, and in the future use RL policies in gazebo
'''
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TwistStamped
from std_srvs.srv import SetBool, Empty, Trigger
# from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.msg import ModelState
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import Config
from dynamic_reconfigure.msg import IntParameter
# import gazebo_msgs
# print(gazebo_msgs)
from sensor_msgs.msg import JointState

import matplotlib.pyplot as plt
import numpy as np
from collections import deque

tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r / 255., g / 255., b / 255.)


data = {'coxas': {'position':deque(maxlen=2000), 'velocity':deque(maxlen=2000), 'effort':deque(maxlen=2000)}}
data.update({'femurs': {'position':deque(maxlen=2000), 'velocity':deque(maxlen=2000), 'effort':deque(maxlen=2000)}})
data.update({'tibias': {'position':deque(maxlen=2000), 'velocity':deque(maxlen=2000), 'effort':deque(maxlen=2000)}})

def jointStateCallback(msg):
    coxa_pos = [0,3,6,9,12,15]
    femur_pos = [1,4,7,10,13,16]
    tibia_pos = [2,5,8,11,14,17]

    data['coxas']['position'].append(np.array(msg.position)[coxa_pos])
    data['coxas']['velocity'].append(np.array(msg.velocity)[coxa_pos])
    data['coxas']['effort'].append(np.array(msg.effort)[coxa_pos])

    data['femurs']['position'].append(np.array(msg.position)[femur_pos])
    data['femurs']['velocity'].append(np.array(msg.velocity)[femur_pos])
    data['femurs']['effort'].append(np.array(msg.effort)[femur_pos])
    
    data['tibias']['position'].append(np.array(msg.position)[tibia_pos])
    data['tibias']['velocity'].append(np.array(msg.velocity)[tibia_pos])
    data['tibias']['effort'].append(np.array(msg.effort)[tibia_pos])
    
def do_plots():
    PATH = '/home/brendan/results/hex_plots/'
    xs = [i for i in range(len(data['coxas']['position']))]
    for name in ['coxas', 'femurs', 'tibias']:
        for thing in ['position', 'velocity', 'effort']:
            f, axes = plt.subplots(6,figsize=(15, 15))
            c = ['g','b','r','c','m','k','y']
            for joint, leg_name in enumerate(['AL', 'AR', 'BL', 'BR', 'CL', 'CR']):
                ys = [i[joint] for i in data[name][thing]]
                min_len = min(len(ys), len(xs))
                axes[joint].plot(xs[:min_len],ys[:min_len],color=tableau20[joint])
                axes[joint].set_title(leg_name)
                # axes[joint].legend(['down = 0 | flat = 1 | up = 2'], loc="upper left")
                # axes[joint].set_ylim([-0.1, 2.3])
           
            plt.savefig(PATH + 'fig_' + name + '_' + thing +  '.png', bbox_inches='tight')
            plt.close(f)
            

def change_gait(gait_number, set_gait_type):

    rospy.wait_for_service('/r1/locomotion_engine_node/set_parameters')
    int_param = IntParameter()
    int_param.name = 'gait_selection'
    int_param.value = gait_number
    reconfigure_service_message = Config()
    reconfigure_service_message.ints = [int_param]
    response = set_gait_type(reconfigure_service_message)
    print(response.config.ints[0])


def run():

    rospy.init_node('runner', anonymous=True)  

    # # Reset robot (at this stage don't reload sim, just move robot, reset joints)
    # rospy.wait_for_service('/gazebo/set_model_state')
    reset_model = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
    model_state = ModelState()
    # print(model_state)
    model_state.model_name = 'r1'
    model_state.pose.position.z = 0.36
    reset_model(model_state)


    # Set to manual velocity override
    # rospy.wait_for_service('/r1/locomotion_engine/manual_velocity_override')
    service_message = SetBool()
    set_manual_velocity_override = rospy.ServiceProxy('/r1/locomotion_engine/manual_velocity_override', service_message)
    data = True
    response = set_manual_velocity_override(data)
    
    # Set forward velocity
    manual_vel_pub = rospy.Publisher('/r1/locomotion_engine/manual_velocity_command', TwistStamped, queue_size=1)
    stamped_msg = TwistStamped()
    stamped_msg.header.stamp = rospy.get_rostime()
    stamped_msg.twist.linear.x = 1.0
    # manual_vel_pub.publish(stamped_msg)

    set_gait_type = rospy.ServiceProxy('/r1/locomotion_engine_node/set_parameters', Reconfigure)
    
    rospy.Subscriber('/r1/joint_states', JointState, jointStateCallback)



    # rospy.wait_for_service('/r1/locomotion_engine/command/walk')
    # walk_message = Trigger()
    # walk_command = rospy.ServiceProxy('/r1/locomotion_engine/command/walk', walk_message)
    # response = walk_command()
    # print(walk_message, response)

#   joint_state_publisher_ = nh_->advertise<sensor_msgs::JointState>("desired_joint_states", 1);

        

    rate = rospy.Rate(100)
    counter = 0
    # gait_number = 4
    # gaits = ['wave', 'ample', 'ripple', 'tripod', 'biped']

    # int_param.value = 0
    # reconfigure_service_message.ints = [int_param]
    # response = set_gait_type(reconfigure_service_message)
    # change_gait(gait_number, set_gait_type)
    while not rospy.is_shutdown():
        if counter % 2000 == 0 and counter != 0:
            do_plots()
        #     gait_number += 1
        #     if gait_number == len(gaits): 
        #         gait_number = 0
        #     rospy.loginfo("Changing gait to: " + gaits[gait_number])
        #     change_gait(gait_number, set_gait_type)
        counter += 1

        # rospy.loginfo(stamped_msg)
        manual_vel_pub.publish(stamped_msg)
        rate.sleep()

    # rospy.loginfo("Setting velocity to 0.0")
    # # rospy.loginfo(stamped_msg)
    # stamped_msg.twist.linear.x = 0.0
    # manual_vel_pub.publish(stamped_msg)



if __name__=='__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass

'''
First: publish robot info to locomotion engine, subscribe to joint commands

locomotion_engine:

# Subscribers:
ros::Subscriber gait_subscriber_;
ros::Subscriber step_frequency_subscriber_;

ros::Subscriber step_default_subscriber_;
ros::Subscriber swing_target_subscriber_;
ros::Subscriber stance_target_subscriber_;
ros::Subscriber joy_subscriber_;

ros::Subscriber tip_state_subscriber_;
ros::Subscriber manual_velocity_subscriber_;
ros::Subscriber auto_velocity_subscriber_;
ros::Subscriber pose_velocity_subscriber_;
ros::Subscriber pose_position_subscriber_;
ros::Subscriber pose_velocity_x_subscriber_;
ros::Subscriber pose_velocity_z_subscriber_;
ros::Subscriber pose_position_z_subscriber_;

# Publishers
ros::Publisher visualisation_publisher_;
ros::Publisher gait_progress_publisher_;
ros::Publisher step_frequency_publisher_;
ros::Publisher stride_publisher_;
ros::Publisher current_body_velocity_publisher_;
ros::Publisher limited_body_velocity_publisher_;
ros::Publisher target_body_velocity_publisher_;
ros::Publisher wbc_tip_states_publisher_;

# Service calls
ros::ServiceServer manual_override_service_server_;
ros::ServiceServer locomotion_pause_service_server_;
ros::ServiceServer walk_behavior_service_server_;
ros::ServiceServer sit_behavior_service_server_;

'''