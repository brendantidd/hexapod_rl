#!/usr/bin/env python
'''
Bridge between ros and pybullet/RL libraries so that we can use lego walking gaits with the hexapod in pybullet, and in the future use RL policies in gazebo
'''
import rospy
import os
from std_msgs.msg import String
from geometry_msgs.msg import TwistStamped
from std_srvs.srv import SetBool, Empty, Trigger
# from gazebo_msgs.msg import ModelState
# from gazebo_msgs.srv import SetModelState
# from gazebo_msgs.msg import ModelState
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import Config
from dynamic_reconfigure.msg import IntParameter
# import gazebo_msgs
# print(gazebo_msgs)
from env import Env
import argparse
import numpy as np

from sensor_msgs.msg import JointState
from sensor_msgs.msg import Imu
import pybullet as p


def change_gait(gait_number, set_gait_type):

    rospy.wait_for_service('/r1/locomotion_engine_node/set_parameters')
    int_param = IntParameter()
    int_param.name = 'gait_selection'
    int_param.value = gait_number
    reconfigure_service_message = Config()
    reconfigure_service_message.ints = [int_param]
    response = set_gait_type(reconfigure_service_message)
    print(response.config.ints[0])


# def stuff_to_publish():
#     '''
#     Locomotion engine needs these things:
#     * /r1/gait [unknown type]
#     * /r1/locomotion_engine/auto_velocity_command [unknown type]
#     * /r1/locomotion_engine/manual_velocity_command [geometry_msgs/TwistStamped]
#     * /r1/locomotion_engine/pose_velocity_command [unknown type]
#     * /r1/locomotion_engine/pose_velocity_z_command [unknown type]
#     * /r1/stance_target [unknown type]
#     * /r1/step_default [unknown type]
#     * /r1/step_frequency [unknown type]
#     * /r1/swing_target [unknown type]
#     * /r1/tip_states [leg_go_ros_msgs/TipStates]

#     Whole body controller needs these;
#     * /r1/body_mass [unknown type]
#     * /r1/desired_body_twist [unknown type]
#     * /r1/desired_impedance_models [unknown type]
#     * /r1/desired_leg_impedance [unknown type]
#     # * /r1/desired_tip_state_single [unknown type]
#     # * /r1/desired_tip_states [leg_go_ros_msgs/TipStates]
#     * /r1/imu/data [unknown type]
#     * /r1/joint_states [unknown type]

#     '''
#     # * /r1/imu/data [unknown type]
#     # * /r1/joint_states [unknown type]
#     # * /r1/tip_states [leg_go_ros_msgs/TipStates]

#     from leg_go_ros_msgs import TipStates

#     # leg_go_ros_msgs/TipStates
#     tip_pub = rospy.Publisher('/r1/tip_states', leg_go_ros_msgs/TipStates, queue_size=1)
#     # sensor_msgs/Imu
#     imu_pub = rospy.Publisher('/r1/imu/data', TwistStamped, queue_size=1)
#     # sensor_msgs/JointState
#     joint_pub = rospy.Publisher('/r1/joint_states', TwistStamped, queue_size=1)
#     stamped_msg = TwistStamped()
#     stamped_msg.header.stamp = rospy.get_rostime()
#     stamped_msg.twist.linear.x = 1.0

def jointStateCallback(data):
    print(data)

def run(args):
    
    if args.hpc:
        PATH = '/home/n8942251/results/' + args.alg + '/' + args.exp + '/'
    else:
        PATH = '/home/brendan/results/' + args.alg + '/' + args.exp + '/'
    
    print("Currently running experiment: ", [i for i in vars(args) if getattr(args, i) == True], "with PATH: ", PATH)
    env = Env()
    env.arguments(RENDER=args.render, PATH=PATH, cur=args.cur, down_stairs=args.down_stairs, up_stairs=args.up_stairs,  variable_start=args.variable_start, add_flat=args.add_flat, disturbances=args.disturbances, colour=args.colour, speed_cur=args.speed_cur, other_rewards=args.other_rewards, control=args.control)

    rospy.init_node('runner', anonymous=True)  

    # sensor_msgs/JointState
    # sensor_msgs/Imu
    imu_pub = rospy.Publisher('/r1/imu/data', Imu, queue_size=1)
    # sensor_msgs/JointState
    joint_pub = rospy.Publisher('/r1/joint_states', JointState, queue_size=1)
    
    rospy.Subscriber('/r1/desired_joint_states', JointState, jointStateCallback)

    obs = env.reset()
    imu_data = Imu()
    # print(imu_data)
    # imu_data.lineaer = env.ob_dict["vx"], env.ob_dict["vy"], env.ob_dict["vz"]
    imu_data.orientation.x = env.qx
    imu_data.orientation.y = env.qy
    imu_data.orientation.z = env.qz
    imu_data.orientation.w = env.qw
    imu_pub.publish(imu_data)
    print(imu_data)
    joint_data = JointState()

    jointStates = p.getJointStates(env.Id,env.ordered_joint_indices)
    for joint, name in zip(jointStates, env.motor_names):
        joint_data.name.append(name)
        joint_data.position.append(joint[0])
        joint_data.velocity.append(joint[1])
        joint_data.effort.append(joint[3])
    joint_pub.publish(joint_data)
    print(joint_data)
    # joint_state

    # Set to manual velocity override
    # rospy.wait_for_service('/r1/locomotion_engine/manual_velocity_override')
    service_message = SetBool()
    set_manual_velocity_override = rospy.ServiceProxy('/r1/locomotion_engine/manual_velocity_override', service_message)
    data = True
    response = set_manual_velocity_override(data)
    
    # Set forward velocity
    manual_vel_pub = rospy.Publisher('/r1/locomotion_engine/manual_velocity_command', TwistStamped, queue_size=1)
    stamped_msg = TwistStamped()
    stamped_msg.header.stamp = rospy.get_rostime()
    stamped_msg.twist.linear.x = 1.0
    # manual_vel_pub.publish(stamped_msg)

    set_gait_type = rospy.ServiceProxy('/r1/locomotion_engine_node/set_parameters', Reconfigure)

    # rospy.wait_for_service('/r1/locomotion_engine/command/walk')
    # walk_message = Trigger()
    # walk_command = rospy.ServiceProxy('/r1/locomotion_engine/command/walk', walk_message)
    # response = walk_command()
    # print(walk_message, response)

#   joint_state_publisher_ = nh_->advertise<sensor_msgs::JointState>("desired_joint_states", 1);

        

    rate = rospy.Rate(1000)
    counter = 0
    gait_number = 0
    gaits = ['wave', 'ample', 'ripple', 'tripod', 'biped']

    # int_param.value = 0
    # reconfigure_service_message.ints = [int_param]
    # response = set_gait_type(reconfigure_service_message)
    change_gait(gait_number, set_gait_type)
    while not rospy.is_shutdown():
        obs, rew, done, _  = env.step(np.zeros(env.ac_size))
        if counter % 1000 == 0 and counter != 0:
            gait_number += 1
            if gait_number == len(gaits): 
                gait_number = 0
            rospy.loginfo("Changing gait to: " + gaits[gait_number])
            # int_param.value = gait_number
            # reconfigure_service_message.ints = [int_param]
            # response = set_gait_type(reconfigure_service_message)
            change_gait(gait_number, set_gait_type)
            # print(response.config.ints[0])
        counter += 1

        # rospy.loginfo(stamped_msg)
        manual_vel_pub.publish(stamped_msg)
        rate.sleep()

    # rospy.loginfo("Setting velocity to 0.0")
    # # rospy.loginfo(stamped_msg)
    # stamped_msg.twist.linear.x = 0.0
    # manual_vel_pub.publish(stamped_msg)



if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--hpc', default=False, action='store_true')
    parser.add_argument('--env', default='env')
    # parser.add_argument('--variable_start', default=False, action='store_true')
    parser.add_argument('--variable_start', default=True, action='store_false')
    # parser.add_argument('--vis', default=False, action='store_true')
    parser.add_argument('--vis', default=True, action='store_false')
    parser.add_argument('--down_stairs', default=False, action='store_true')
    parser.add_argument('--up_stairs', default=False, action='store_true')
    parser.add_argument('--speed_cur', default=False, action='store_true')
    # parser.add_argument('--control', default=False, action='store_true')
    parser.add_argument('--control', default=True, action='store_false')
    parser.add_argument('--add_flat', default=False, action='store_true')
    parser.add_argument('--cur', default=False, action='store_true')
    parser.add_argument('--other_rewards', default=False, action='store_true')
    parser.add_argument('--sym', default=False, action='store_true')
    # parser.add_argument('--disturbances', default=False, action='store_true')
    parser.add_argument('--disturbances', default=True, action='store_false')
    parser.add_argument('--test', default=False, action='store_true')
    parser.add_argument('--render', default=False, action='store_true')
    parser.add_argument('--no_gpu', default=False, action='store_true')
    parser.add_argument('--entropy', default=False, action='store_true')
    parser.add_argument('--colour', default=False, action='store_true')
    parser.add_argument('--enforce_weights', default=False, action='store_true')
    parser.add_argument('--seed', type=int, default=42)
    parser.add_argument('--alg', default='hex')
    parser.add_argument('--exp', default='')
    # args = parser.parse_args()
    args, unknown = parser.parse_known_args()
    if args.no_gpu:
        os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    try:
        run(args)
    except rospy.ROSInterruptException:
        pass


'''
 * /r1/gait [unknown type]
 * /r1/locomotion_engine/auto_velocity_command [unknown type]
 * /r1/locomotion_engine/manual_velocity_command [geometry_msgs/TwistStamped]
 * /r1/locomotion_engine/pose_velocity_command [unknown type]
 * /r1/locomotion_engine/pose_velocity_z_command [unknown type]
 * /r1/stance_target [unknown type]
 * /r1/step_default [unknown type]
 * /r1/step_frequency [unknown type]
 * /r1/swing_target [unknown type]
 * /r1/tip_states [leg_go_ros_msgs/TipStates]
'''


'''
First: publish robot info to locomotion engine, subscribe to joint commands

locomotion_engine:

# Subscribers:
ros::Subscriber gait_subscriber_;
ros::Subscriber step_frequency_subscriber_;

ros::Subscriber step_default_subscriber_;
ros::Subscriber swing_target_subscriber_;
ros::Subscriber stance_target_subscriber_;
ros::Subscriber joy_subscriber_;

ros::Subscriber tip_state_subscriber_;
ros::Subscriber manual_velocity_subscriber_;
ros::Subscriber auto_velocity_subscriber_;
ros::Subscriber pose_velocity_subscriber_;
ros::Subscriber pose_position_subscriber_;
ros::Subscriber pose_velocity_x_subscriber_;
ros::Subscriber pose_velocity_z_subscriber_;
ros::Subscriber pose_position_z_subscriber_;

# Publishers
ros::Publisher visualisation_publisher_;
ros::Publisher gait_progress_publisher_;
ros::Publisher step_frequency_publisher_;
ros::Publisher stride_publisher_;
ros::Publisher current_body_velocity_publisher_;
ros::Publisher limited_body_velocity_publisher_;
ros::Publisher target_body_velocity_publisher_;
ros::Publisher wbc_tip_states_publisher_;

# Service calls
ros::ServiceServer manual_override_service_server_;
ros::ServiceServer locomotion_pause_service_server_;
ros::ServiceServer walk_behavior_service_server_;
ros::ServiceServer sit_behavior_service_server_;

'''