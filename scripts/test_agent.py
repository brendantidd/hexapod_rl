import sys
sys.path.append('../../brendan')
# from assets.env_simple import Env
# print("wht")
from env import Env
# from assets.env_old2 import Env
# from subpolicies.env import Env
import pybullet as p
import time
import numpy as np
from numpy import cos as c
from numpy import sin as s

env = Env()
env.arguments(RENDER=True, PATH=None, record_step = False)
obs = env.reset()

angles = ['roll', 'pitch', 'yaw']
angle_target = {name: {'target': p.addUserDebugParameter(name + "_target",-.5,.5,0)} for name in angles}
joint_target = {name: {'target': p.addUserDebugParameter(name + "_target",-2.0,2.0,0)} for name in env.motor_names}

pos = ['x', 'y', 'z']
pos_target = {name: {'target': p.addUserDebugParameter(name + "_target",-1.5,1.5,0)} for name in pos if name in ['x', 'y']}
pos_target['z'] = {'target': p.addUserDebugParameter("z_target",-1.5,1.5,0.5)}

# cam_target = {}
# cam_target['dx'] = {'target': p.addUserDebugParameter("dx_target",-1.5,1.5,0.6)}
# cam_target['dy'] = {'target': p.addUserDebugParameter("dy_target",-1.5,1.5,0.0)}
# cam_target['dz'] = {'target': p.addUserDebugParameter("dz_target",-1.5,1.5,0.-0.05)}
# cam_target['cam_x'] = {'target': p.addUserDebugParameter("cam_x_target",-1.5,1.5,0.45)}
# cam_target['cam_y'] = {'target': p.addUserDebugParameter("cam_y_target",-1.5,1.5,0.0)}
# cam_target['cam_z'] = {'target': p.addUserDebugParameter("cam_z_target",-1.5,1.5,0.0)}



# insert_floor()

actions = np.zeros(18)
vels = ['vx', 'vy', 'vz', 'vx_swing', 'vy_swing', 'vz_swing']
vel_dict = {v:0.0 for v in vels}
roll, pitch, yaw = 0.0, 0.0, 0.0
actions = np.zeros(18)
while (1):
	time.sleep(0.01)

	roll = p.readUserDebugParameter(angle_target['roll']['target'])
	pitch = p.readUserDebugParameter(angle_target['pitch']['target'])
	yaw = p.readUserDebugParameter(angle_target['yaw']['target'])

	x = p.readUserDebugParameter(pos_target['x']['target'])
	y = p.readUserDebugParameter(pos_target['y']['target'])
	z = p.readUserDebugParameter(pos_target['z']['target'])
	
	# dx = p.readUserDebugParameter(cam_target['dx']['target'])
	# dy = p.readUserDebugParameter(cam_target['dy']['target'])
	# dz = p.readUserDebugParameter(cam_target['dz']['target'])
	# cam_x = p.readUserDebugParameter(cam_target['cam_x']['target'])
	# cam_y = p.readUserDebugParameter(cam_target['cam_y']['target'])
	# cam_z = p.readUserDebugParameter(cam_target['cam_z']['target'])
	
	orn = p.getQuaternionFromEuler([roll, pitch, yaw])

	for i,j in enumerate(env.motor_names):
		actions[i] = p.readUserDebugParameter(joint_target[j]['target'])

	env.set_position([x,y,z], orn, actions)
	# env.set_position([0,0,1], orn)


	vel = np.array([vel_dict[v] for v in vels])
	# ob, rew_llc, rew_hlc, done, _ = env.step(np.zeros(12), vel)
	ob, rew_hlc, done, _ = env.step(np.zeros(18))

	# env.get_debug_image(display=True)
	# env.get_image([dx, dy, dz], [cam_x, cam_y, cam_z])
	env.get_observation()
	# print(env.ob_dict['left_foot_y'], env.ob_dict['right_foot_y'])
	# print(env.ob_dict['left_foot_x'], env.ob_dict['right_foot_x'])
    # pos, q = p.getBasePositionAndOrientation(Id)
    # euler = p.getEulerFromQuaternion(q)
    # print(roll, pitch, yaw, euler)

	# p.stepSimulation()
