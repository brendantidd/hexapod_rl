'''
Actuator model
Input:
velocity history and position history
Output:
Torque
'''

import utils as U
import tensorflow as tf
import numpy as np

class Network(object):
    def __init__(self, name, *args, **kwargs):
        with tf.variable_scope(name):
            self._init(*args, **kwargs)
            self.scope = tf.get_variable_scope().name

    def _init(self, input_ph):
        # with tf.variable_scope("obfilter"):
        #     self.ob_rms = RunningMeanStd(shape=ob_space.shape)
        # obz = tf.clip_by_value((ob - self.ob_rms.mean) / self.ob_rms.std, -5.0, 5.0)
        last_out = tf.nn.softsign(tf.layers.dense(last_out, 32, name="fc1"))
        last_out = tf.nn.softsign(tf.layers.dense(last_out, 32, name="fc2"))
        last_out = tf.nn.softsign(tf.layers.dense(last_out, 32, name="fc3"))
        self.pred = tf.layers.dense(last_out, 1, name='final')[:,0]
