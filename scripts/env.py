import sys
sys.path.append('../../brendan')
import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import pybullet as p
import gym
from gym import spaces
import time
import numpy as np
from numpy import cos as c
from numpy import sin as s
from gym.utils import seeding
from collections import deque
import cv2
import time
from subpolicies.obstacles import Obstacles
import copy
import random

class Env(gym.Env):
    # 44 state, 1 vx control
    ob_size = 61
    ac_size = 18
    timeStep = 0.005
    initial_reward_flag = True
    initial_reward = 0
    total_reward = 0
    Kp = Kd = 400
    Kd_x = Kp_coxa = Kd
    steps = 0
    total_steps = 0
    actionRepeat = 1
    box_info = None
    height_coeff = 0.0
    # height_coeff = 0.1
    step_length = 0.6
    speed = 2.0
    speed_range = 0
    # height_coeff = 0.1
    frict_coeff = 0.0
    # length_coeff = 0.0
    length_coeff = 0.3
    fov = 60
    nearPlane = 0.01
    farPlane = 1000
    aspect = 1
    episodes = -1
    projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane);
    def __init__(self):
        pass
    def arguments(self, RENDER=False, PATH=None, MASTER=True, cur=False, down_stairs=False, up_stairs=False, disturbances=False, record_step=True, camera_rotation=80, variable_start=False, add_flat=False, colour=False, y_offset=0.0, z_offset=0.0, speed_cur=False, other_rewards = False, control=True, up_down_flat=False, args=None):
        # if up_stairs or down_stairs:
        #     self.control = False
        # else:
        self.control = control
        if self.control:
            self.ob_size += 1 
        self.args = args
        
        high = 5*np.ones(self.ac_size)
        low = -high
        self.action_space  = spaces.Box(low, high)

        high = np.inf*np.ones(self.ob_size)
        low = -high
        self.observation_space = spaces.Box(low,high)
        # self.arguments(False, False, '4')
        self.colour = colour
        self.render = RENDER
        self.record_step = record_step
        self.camera_rotation = camera_rotation
        self.PATH = PATH
        self.cur = cur
        self.down_stairs = down_stairs
        self.up_stairs = up_stairs
        self.speed_cur = speed_cur
        self.other_rewards = other_rewards
        self.disturbances = disturbances
        self.variable_start = variable_start
        self.up_down_flat = up_down_flat
        self.add_flat = add_flat
        self.y_offset = y_offset
        self.z_offset = z_offset
        self.obstacles = Obstacles(y_offset=self.y_offset, add_friction=None, colour=self.colour)
        self.MASTER = MASTER
        if self.render and self.MASTER:
        	self.physicsClientId = p.connect(p.GUI)
        else:
            self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the running gait
        self.sim_data = []
        self.best_reward = -100

        # Right and Left Joint positions, velocities, comz position, base velocities, orientation and velocity, feet contacts, prev feet contacts
        self.state = [  "AR_coxa_joint_pos","AR_femur_joint_pos","AR_tibia_joint_pos",
                        "AL_coxa_joint_pos","AL_femur_joint_pos","AL_tibia_joint_pos",
                        "BR_coxa_joint_pos","BR_femur_joint_pos","BR_tibia_joint_pos",
                        "BL_coxa_joint_pos","BL_femur_joint_pos","BL_tibia_joint_pos",
                        "CR_coxa_joint_pos","CR_femur_joint_pos","CR_tibia_joint_pos",
                        "CL_coxa_joint_pos","CL_femur_joint_pos","CL_tibia_joint_pos"]
        self.state += [ "AR_coxa_joint_vel","AR_femur_joint_vel","AR_tibia_joint_vel",
                        "AL_coxa_joint_vel","AL_femur_joint_vel","AL_tibia_joint_vel",
                        "BR_coxa_joint_vel","BR_femur_joint_vel","BR_tibia_joint_vel",
                        "BL_coxa_joint_vel","BL_femur_joint_vel","BL_tibia_joint_vel",
                        "CR_coxa_joint_vel","CR_femur_joint_vel","CR_tibia_joint_vel",
                        "CL_coxa_joint_vel","CL_femur_joint_vel","CL_tibia_joint_vel"]

        self.state += ['com_z']
        self.state += ['vx','vz','pitch','pitch_vel']

        # Needs negatives when inverting:
        self.state += ['vy','roll','yaw']
        self.state += ['roll_vel','yaw_vel']

        # Not continous values, don't normalise ================================
        self.state += ["AR_foot_link","AL_foot_link","BR_foot_link",
                        "BL_foot_link","CR_foot_link","CL_foot_link"]
        self.state += ["prev_AR_foot_link","prev_AL_foot_link","prev_BR_foot_link",
                        "BL_foot_link","prev_CR_foot_link","prev_CL_foot_link"]
        
        self.state += ['left_foot_on_ground', 'right_foot_on_ground']
        self.state += ['swing_foot']
        
        
        # Previous trajectory targets
        # self.state += ['ready_to_walk']
        # Y states, and roll and yaw that need to be negatived..
        # States that need to have logic switched: State flags, left = 0, right = 1
        # self.state += ['swing_foot']
        #=======================================================================
        # State flags, left = 0, right = 1
        # ======================================================================
        # self.sample_nb = 6
        # self.sample_max = self.sample_nb
        self.saved_state = []

    def reset(self, door_data=None):
        # print("===============================================")
        # print(self.prev_speed, self.speed)
        self.foot_count = np.zeros(6)

        self.prev_actions = np.zeros(self.ac_size)
        self.episodes += 1
        self.cur_foot = 'right'
        self.left_on_box = 0.0
        self.right_on_box = 0.0
        self.min_v = 0.5
        self.max_v = 2.0
        if self.speed_cur:
            self.speed = np.random.uniform(low=self.min_v,high=self.max_v)
        
        self.step_param = 30
        self.time_of_step = int(self.step_param/self.speed)
        # self.time_of_step = int(100/self.speed)
        self.vel_buffer = deque(maxlen=self.time_of_step)
        self.action_store = deque(maxlen=self.time_of_step)
        # self.height_coeff = 0.05
        if self.up_stairs or self.down_stairs:
            if (self.steps*self.timeStep) > 6 and self.steps != 0:
                # if self.height_coeff < 0.1:
                if self.height_coeff < 0.05:
                    self.height_coeff += 0.005
        
        if (self.steps*self.timeStep) > 6 and self.steps != 0:
                self.Kp_coxa *= 0.9
                if self.Kp_coxa < 10: self.Kp_coxa = 0
        if self.cur:
            
            # print(self.steps*self.timeStep)
            if (self.steps*self.timeStep) > 6 and self.steps != 0:
                self.Kp = self.Kd = self.Kd_x = self.Kp*0.75
                if self.Kp < 10:
                    self.Kd_x = self.Kp = self.Kd = 0
                    self.cur = False

        if self.record_step:
            if self.best_reward < self.total_reward and self.total_reward != 0:
                self.best_reward = self.total_reward
                self.save_sim_data(best=True)
                self.save_sim_data()
            else:
                self.save_sim_data(best=False)
            self.sim_data = []
        # ======================================================================
        # if self.variable_start and self.episodes % 2 == 0 and self.episodes != 0:
        if self.variable_start and self.episodes != 0:
            self.restore()
        else:
            p.resetSimulation()
            self.load_model()
        if self.colour:
            self.obstacles.set_colour(self.box_info[0][self.box_num], self.speed)
            self.obstacles.set_colour(self.box_info[0][self.box_num + 1], self.speed)
        self.steps = 0
        self.total_reward = 0
        self.prev_actions = np.zeros(self.ac_size)
        self.zeros = np.zeros(0)
        self.get_observation()
        if self.control:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed]))
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state]))

    def load_model(self):

        if self.MASTER: p.loadMJCF(currentdir + "/ground.xml")
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        # print(currentdir + "/robot.urdf")
        # print(currentdir + "/biped.urdf")
        # objs = p.loadURDF(currentdir + "/biped.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION + p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        # objs = p.loadURDF(currentdir + "/robot.urdf")
        # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION)
        # print(objs)
        self.Id = objs
        # print(objs)
        p.setTimeStep(self.timeStep)
        p.setGravity(0,0,-9.8)
        # ======================================================================
        # time.sleep(5)
        numJoints = p.getNumJoints(self.Id)
        
        # Camera following robot:
        # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
        # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
        # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

        self.jdict = {}
        self.feet_dict = {}
        self.leg_dict = {}

        # self.feet = ["left_heel1", "left_heel2", "right_heel1", "right_heel2", "left_toe", "right_toe"]
        self.feet = ["AR_foot_link","AL_foot_link","BR_foot_link",
                    "BL_foot_link","CR_foot_link","CL_foot_link"]
        
        self.feet_contact = {f:True for f in self.feet}
        self.ordered_joints = []
        self.ordered_joint_indices = []
        for j in range( p.getNumJoints(self.Id) ):
            info = p.getJointInfo(self.Id, j)
            link_name = info[12].decode("ascii")
            if link_name in self.feet: self.feet_dict[link_name] = j
            self.ordered_joint_indices.append(j)
            if info[2] != p.JOINT_REVOLUTE: continue
            jname = info[1].decode("ascii")
            # print(jname)
            lower, upper = (info[8], info[9])
            p.enableJointForceTorqueSensor(self.Id, j, True)
            self.ordered_joints.append( (j, lower, upper) )
            self.jdict[jname] = j
        
        self.motor_names = [
                            "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
                            "AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
                            "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
                            "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
                            "CR_coxa_joint","CR_femur_joint","CR_tibia_joint",
                            "CL_coxa_joint","CL_femur_joint","CL_tibia_joint"]

        self.motor_power = [300, 300, 300, 300, 300, 300]
        self.motor_power += [300, 300, 300, 300, 300, 300]
        self.motor_power += [300, 300, 300, 300, 300, 300]
    
        self.motors = [self.jdict[n] for n in self.motor_names]
        forces = np.ones(len(self.motors))*240
        self.actions = {key:0.0 for key in self.motor_names}

        # Disable motors to use torque control:
        p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

        # Increase the friction on the feet, we have heaps of friction at our feet.
        for key in self.feet_dict:
            p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)

        if self.up_stairs:
            up_down_flat_arg = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(40)]
            self.box_info = self.obstacles.up_down_flat(height_coeff = self.height_coeff, rand=False, order=up_down_flat_arg)            
            # self.box_info = self.obstacles.add_stairs(length_coeff = self.length_coeff, height_coeff = self.height_coeff, add_flat=self.add_flat, disturb=self.disturbances, speed=self.speed, mode='up')
        elif self.down_stairs:
            self.box_info = self.obstacles.add_stairs(length_coeff = self.length_coeff, height_coeff = self.height_coeff, add_flat=self.add_flat, disturb=self.disturbances, speed=self.speed, mode='down')
        elif self.up_down_flat:
            self.box_info = self.obstacles.up_down_flat(height_coeff = self.height_coeff, rand=False, order=self.up_down_flat_arg)
        else:
            # if self.args.display:
            self.box_info = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]], [[0,0,0],[0,0,0]], [[0,0,0],[0,0,0]]] 
            # else:      
            #     self.box_info = self.obstacles.add_stairs(length_coeff = self.length_coeff, height_coeff = 0.0, add_flat=self.add_flat, disturb=self.disturbances, speed=self.speed, mode='flat')
        self.box_num = 0

        self.body_xyz, _ = p.getBasePositionAndOrientation(self.Id)
        if self.down_stairs or self.up_stairs or self.up_down_flat:
            if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
                self.box_num += 1
                self.stepped_on_box = False
                self.second_step_on_box = False
            if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
                self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
            else:
                self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        self.restore()

    def restore(self, sample=None):
        # Restore state every second time (need to always pick a state to restore starting from 0).
        # print(self.variable_start and self.episodes % 2 == 0 and self.episodes != 0 and self.saved_state)
        if self.variable_start and self.episodes % 2 == 0 and self.episodes != 0 and self.saved_state:
            pos, orn, joints, base_vel, joint_vel, saved_dict, others = self.saved_state
            self.ob_dict = saved_dict
            self.box_num, self.cur_foot, self.steps = others
            # [pos, orn, rot, vel, joint, joint_vel]
            # c = [0.01, 0.001, 0.01, 0.01, 0.0, 0.0]
            # c = [0.015, 0.006, 0.012, 0.4, 0.01, 0.01]
            # c = [0.015, 0.006, 0.012, 0.4, 0.25, 0.2]
            c = [0.015, 0.006, 0.012, 0.4, 0.15, 0.1]
            self.saved_state = []
        else:
            self.ob_dict = {"AR_foot_link":True,"AL_foot_link":True,"BR_foot_link":True,
                        "BL_foot_link":True,"CR_foot_link":True,"CL_foot_link":True}
            self.ob_dict.update({"prev_AR_foot_link":True,"prev_AL_foot_link":True,"prev_BR_foot_link":True,
                        "prev_BL_foot_link":True,"prev_CR_foot_link":True,"prev_CL_foot_link":True})
            self.ob_dict.update({"left_foot_on_ground":True,"right_foot_on_ground":True})
            self.ob_dict.update({'roll':0.0,'pitch':0.0,'yaw':0.0})
            self.ob_dict['ready_to_walk'] = False
            self.ob_dict['swing_foot'] = 1
            self.cur_foot = 'right'
            pos, orn, joints, base_vel, joint_vel = [0,0,self.z_offset+0.35],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            # pos, orn, joints, base_vel, joint_vel = [0.5,0,self.z_offset+0.4],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            self.sample = random.sample([_ for _ in range(150)], 1)[0]
            self.save_state = []
            # [pos, orn, rot, vel, joint, joint_vel]
            # c = [0.015, 0.006, 0.012, 0.4, 0.01, 0.01]
            # c = [0.015, 0.006, 0.012, 0.4, 0.15, 0.1]
            c = [0.015, 0.006, 0.012, 0.4, 0.005, 0.01]

        if self.disturbances:
            pos[0] += np.random.uniform(low=-c[0], high=c[0])
            pos[1] += np.random.uniform(low=-c[0], high=c[0])
            # pos[2] += np.random.uniform(low=-c[0], high=c[0])
            orn[0] += np.random.uniform(low=-c[1], high=c[1])
            orn[1] += np.random.uniform(low=-c[1], high=c[1])
            orn[2] += np.random.uniform(low=-c[1], high=c[1])
            orn[3] += np.random.uniform(low=-c[1], high=c[1])
            for i in range(len(base_vel[0])):
                base_vel[0][i] += np.random.uniform(low=-c[2], high=c[2])
                base_vel[1][i] += np.random.uniform(low=-c[3], high=c[3])
            for i, j in enumerate(self.ordered_joints):
                joints[i] += np.clip(np.random.uniform(low=-c[4], high=c[4]), j[1], j[2])
                joint_vel[i] += np.random.uniform(low=-c[5], high=c[5])
        self.set_position(pos, orn, joints, base_vel, joint_vel)

    def get_sample(self):
        pos = list(self.body_xyz)
        orn = [self.qx, self.qy, self.qz, self.qw]
        joints = [self.ob_dict[m + '_pos'] for m in self.motor_names]
        joint_vel = [self.ob_dict[m + '_vel'] for m in self.motor_names]
        base_vel = [list(self.body_vxyz), list(self.base_rot_vel)]
        saved_dict = self.ob_dict
        others = [self.box_num, self.cur_foot, self.steps]
        self.saved_state = copy.deepcopy([pos, orn, joints, base_vel, joint_vel, saved_dict, others])
        return self.saved_state

    def seed(self, seed=None):
    	self.np_random, seed = seeding.np_random(seed)
    	return [seed]

    def close(self):
    	print("closing")

    def step(self, actions):
        # print(self.steps*self.timeStep)
        # if (self.steps*self.timeStep) % 6 == 0:
        if self.speed_cur and (self.steps*self.timeStep) % 1.5 == 0 and self.steps != 0:
            self.speed = np.random.uniform(low=self.min_v,high=self.max_v)
            # self.speed = np.random.uniform(low=1.25-self.speed_range,high=np.clip(1.25+self.speed_range, a_min=None,a_max=1.5))
            if self.colour:
                for box in range(self.box_num, len(self.box_info[0])):
                    self.obstacles.set_colour(self.box_info[0][box], self.speed)
            # if self.steps > 0:
            #     if self.speed_range < 1.25: self.speed_range += 0.01
                # print("desired speed ", round(self.speed,2), "actual ", round(np.mean(self.vel_buffer),2), "min ", 1.25-self.speed_range, "max ", 1.25+self.speed_range, self.steps)
            self.time_of_step = int(self.step_param/self.speed)
            self.vel_buffer = deque(maxlen=self.time_of_step)
            self.action_store = deque(maxlen=self.time_of_step)
        # if (self.steps*self.timeStep) % 4.5 == 0 and self.steps != 0 and (self.up_stairs or self.down_stairs):
        #     if self.height_coeff < 0.1:
        #         self.height_coeff += 0.0001

        if self.steps > 150 and self.disturbances and random.random() < 0.02:
            self.add_disturbance()
        t1 = time.time()
        actions = np.clip(actions, -10, 10)
        if self.cur:
            actions = self.apply_forces(actions)
            # self.apply_forces(actions)
        for _ in range(self.actionRepeat):
            forces = [0.] * len(self.motor_names)
            for m in range(len(self.motor_names)):
                forces[m] = self.motor_power[m]*actions[m]*0.082
            p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)
            # p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=actions, forces=self.motor_power)
            if self.MASTER:
                p.stepSimulation()
        self.get_observation()
        self.vel_buffer.append(self.ob_dict['vx'])
        if self.down_stairs or self.up_stairs or self.up_down_flat:
            if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
                self.box_num += 1
                self.stepped_on_box = False
                self.second_step_on_box = False
            if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
                self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
            else:
                self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]

        reward, done = self.reward(actions)
        self.total_reward += reward
        if self.record_step: self.record_sim_data()
        if self.variable_start and self.steps == self.sample and not done:
            self.get_sample()
        self.steps += 1
        self.total_steps += 1
        if self.render:
          time.sleep(0.01)

        if self.control:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed])), reward, done, self.ob_dict
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state])), reward, done, self.ob_dict

    def reward(self, actions):
        '''
        Positive - higher functionality: com velocity tracking
        Negative - 0 if following experts distinct features:
        - time of step
        - joint limits
        '''
        done = False
        if self.steps > 2000: done = True
        # if self.steps > 1000: done = True
        # if (self.body_xyz[2] < (0.25 + self.z_offset)):
        # if (self.body_xyz[2] < (0.15 + self.z_offset) or abs(self.body_xyz[1]) > 0.5 ):
            # done = True
        # print(self.body_xyz[2])
        # if self.steps > 5000: done = True
        if self.args.sideways: 
            x = abs(self.ob_dict['vy'] - self.speed)
            if (self.body_xyz[2] < (0.25 + self.z_offset) or abs(self.body_xyz[0]) > 0.3 ):     
                done = True
        else:
            x = abs(self.ob_dict['vx'] - self.speed)
            if (self.body_xyz[2] < (0.25 + self.z_offset) or abs(self.body_xyz[1]) > 0.3 ):     
                done = True

        # reward = (1/(np.e**x + np.e**-x))*(1.5/0.5)
        reward = (3/(np.e**(3*x) + np.e**(-3*x)))
        # reward = min(self.ob_dict['vx'], self.speed)
        # reward -= 0.001*np.square(actions).sum()
        # print("actions: ", 0.005*np.square(actions).sum())
        # Keep com steady/no anglular displacement
        reward -= abs(self.ob_dict['pitch']) + abs(self.ob_dict['roll']) + abs(self.ob_dict['yaw'])

        
        # # print(abs(self.ob_dict['pitch']), abs(self.ob_dict['roll']), abs(self.ob_dict['yaw']))
        
        # # Height pen.. 
        # # reward -= (self.body_xyz[2] - 0.4)**2
        # # print("y_pen", 5*(self.body_xyz[2] - 0.35)**2)
        
        # # if not self.ob_dict['prev_AR_foot_link_left_ground'] and self.ob_dict['AR_foot_link_left_ground']: self.foot_count[0] += 1
        # # if not self.ob_dict['prev_AL_foot_link_left_ground'] and self.ob_dict['AL_foot_link_left_ground']: self.foot_count[1] += 1
        # # if not self.ob_dict['prev_BR_foot_link_left_ground'] and self.ob_dict['BR_foot_link_left_ground']: self.foot_count[2] += 1
        # # if not self.ob_dict['prev_BL_foot_link_left_ground'] and self.ob_dict['BL_foot_link_left_ground']: self.foot_count[3] += 1
        # # if not self.ob_dict['prev_CR_foot_link_left_ground'] and self.ob_dict['CR_foot_link_left_ground']: self.foot_count[4] += 1
        # # if not self.ob_dict['prev_CL_foot_link_left_ground'] and self.ob_dict['CL_foot_link_left_ground']: self.foot_count[5] += 1
        # # # print(np.array(self.foot_count>1).any())
        # # if (self.foot_count > 5).any(): 
        # # #     # done = True        
        #     # reward -= 0.01
        #     # reward -= 0.0
        # # #     # print("double touch", self.foot_count)
        # # if self.foot_change and (self.foot_count==0).any(): 
        # #     self.zeros = np.where(self.foot_count==0)[0]
        # #     # print(self.zeros)
        # # if self.zeros.shape[0] > 0 and (self.foot_count[self.zeros]==0).any():
        # # #     # done = True  
        # #     reward -= 0.5    
        # #     # print(-0.5)  
            
        #     # print("none touched", self.foot_count)



        # # tib_rew = abs(pre_reward - reward)
        
        # #  Encourage mirroring of coxa movements.
        # # reward -= 0.1*(self.ob_dict['AR_coxa_joint_pos'] - self.ob_dict['AL_coxa_joint_pos']) **2
        # # reward -= 0.1*(self.ob_dict['BR_coxa_joint_pos'] - self.ob_dict['BL_coxa_joint_pos']) **2
        # # reward -= 0.1*(self.ob_dict['CR_coxa_joint_pos'] - self.ob_dict['CL_coxa_joint_pos']) **2
        # # print(pre_reward - reward)

        # # Smoothness cost:
        # # reward -= 0.005*np.sum((actions - self.prev_actions)**2)
        # # self.prev_actions = actions

        # # # Encourage tripod, if left side not off ground and right side not off ground:        
        # if self.ob_dict['swing_foot']:
        #     stance = 'left'; swing = 'right'
        # else:
        #     stance = 'right'; swing = 'left'
        
        # # fact1 = 0.05
        # # prev_rew = reward
        # # reward -= fact1*(self.ob_dict["AR_coxa_joint_pos"] + self.ob_dict["BL_coxa_joint_pos"] - self.ob_dict["CR_coxa_joint_pos"])**2
        # # reward -= fact1*(self.ob_dict["AL_coxa_joint_pos"] + self.ob_dict["BR_coxa_joint_pos"] - self.ob_dict["CL_coxa_joint_pos"])**2
        # # reward -= fact1*(self.ob_dict["AR_femur_joint_pos"] - self.ob_dict["BL_femur_joint_pos"] - self.ob_dict["CR_femur_joint_pos"])**2
        # # reward -= fact1*(self.ob_dict["AL_femur_joint_pos"] - self.ob_dict["BR_femur_joint_pos"] - self.ob_dict["CL_femur_joint_pos"])**2       
        # # reward -= fact1*(self.ob_dict["AR_tibia_joint_pos"] - self.ob_dict["BL_tibia_joint_pos"] - self.ob_dict["CR_tibia_joint_pos"])**2
        # # reward -= fact1*(self.ob_dict["AL_tibia_joint_pos"] - self.ob_dict["BR_tibia_joint_pos"] - self.ob_dict["CL_tibia_joint_pos"])**2
        # # fact1 = 0.01
        # # prev_rew = reward
        # # reward -= fact1*(self.ob_dict["AR_coxa_joint_pos"]  - self.ob_dict["CL_coxa_joint_pos"])**2
        # # reward -= fact1*(self.ob_dict["AL_coxa_joint_pos"]  - self.ob_dict["CR_coxa_joint_pos"])**2
        # # reward -= fact1*(self.ob_dict["BR_coxa_joint_pos"]  - self.ob_dict["BL_coxa_joint_pos"])**2
        # # reward -= fact1*((self.ob_dict["AR_coxa_joint_pos"]-0.55)  - (self.ob_dict["AL_coxa_joint_pos"]+0.55))**2
        # # reward -= fact1*((self.ob_dict["CR_coxa_joint_pos"]+0.55)  - (self.ob_dict["CL_coxa_joint_pos"]-0.55))**2
        


        # # factor = 0.5
        # # # right = positive forward, left = negative forward
        # # cox_max = 0.15
        # # cox_min = -0.15
        # # pre_reward = reward
        # # if self.steps % self.time_of_step > 0.75*self.time_of_step:
        # #     if swing == 'right':
        # #         reward -= factor*(self.ob_dict['AR_coxa_joint_pos'] - cox_max)**2
        # #         reward -= factor*(self.ob_dict['BR_coxa_joint_pos'] - cox_min)**2
        # #         reward -= factor*(self.ob_dict['CR_coxa_joint_pos'] - cox_max)**2
        # #         reward -= factor*(self.ob_dict['AL_coxa_joint_pos'] - cox_max)**2
        # #         reward -= factor*(self.ob_dict['BL_coxa_joint_pos'] - cox_min)**2
        # #         reward -= factor*(self.ob_dict['CL_coxa_joint_pos'] - cox_max)**2
        # #     elif swing == 'left':
        # #         reward -= factor*(self.ob_dict['AR_coxa_joint_pos'] - cox_min)**2
        # #         reward -= factor*(self.ob_dict['BR_coxa_joint_pos'] - cox_max)**2
        # #         reward -= factor*(self.ob_dict['CR_coxa_joint_pos'] - cox_min)**2
        # #         reward -= factor*(self.ob_dict['AL_coxa_joint_pos'] - cox_min)**2
        # #         reward -= factor*(self.ob_dict['BL_coxa_joint_pos'] - cox_max)**2
        # #         reward -= factor*(self.ob_dict['CL_coxa_joint_pos'] - cox_min)**2
        # # print(self.steps % self.time_of_step, 0.6*self.time_of_step)
        # # cox_rew = abs(pre_reward - reward)

        # # # right = positive forward, left = negative forward
        # # factor = 1.5
        # # factor = 1.5
        # factor = 1.0
        # # factor = 0.8
        # # cox_max = 0.05
        # # cox_min = -0.05
        # pre_reward = reward
        # max_AC = 0.6
        # min_AC = 0.3
        # max_B = 0.1
        # min_B = -0.1
        # if self.steps % self.time_of_step > (0.90*self.time_of_step):
        #     if swing == 'right':
        #         reward -= factor if (self.ob_dict['AR_coxa_joint_pos'] < max_AC) else 0
        #         reward -= factor if (self.ob_dict['BR_coxa_joint_pos'] > min_B) else 0
        #         reward -= factor if (self.ob_dict['CR_coxa_joint_pos'] < -min_AC) else 0
        #         reward -= factor if (self.ob_dict['AL_coxa_joint_pos'] < -min_AC) else 0
        #         reward -= factor if (self.ob_dict['BL_coxa_joint_pos'] > min_B) else 0
        #         reward -= factor if (self.ob_dict['CL_coxa_joint_pos'] < max_AC) else 0
        #     elif swing == 'left':
        #         reward -= factor if (self.ob_dict['AR_coxa_joint_pos'] > min_AC) else 0
        #         reward -= factor if (self.ob_dict['BR_coxa_joint_pos'] < max_B) else 0
        #         reward -= factor if (self.ob_dict['CR_coxa_joint_pos'] > -max_AC) else 0
        #         reward -= factor if (self.ob_dict['AL_coxa_joint_pos'] > -max_AC) else 0
        #         reward -= factor if (self.ob_dict['BL_coxa_joint_pos'] < max_B) else 0
        #         reward -= factor if (self.ob_dict['CL_coxa_joint_pos'] > min_AC) else 0
        # else:
        #     # Most of the time want swing foot off the ground, stance foot on the ground
        #     if not self.ob_dict[stance + '_foot_on_ground']:
        #         # reward -= 0.5
        #         reward -= 0.05
        #     if self.ob_dict[swing + '_foot_on_ground']:
        #         # reward -= 0.5
        #         reward -= 0.05
        #         # # leg_sym_rew = abs(reward - prev_rew)
        # # # If no feet on ground.
        # if not self.ob_dict['left_foot_on_ground'] and not self.ob_dict['right_foot_on_ground'] and self.steps > 50:
        #     reward -= 0.5
        
        
        # # print(swing)
        # # factor = 0.05
        # # tol = 0.1
        # # reward -= factor if self.ob_dict['AR_coxa_joint_pos'] > (max_AC + tol) or self.ob_dict['AR_coxa_joint_pos'] < (min_AC - tol) else 0
        # # reward -= factor if self.ob_dict['BR_coxa_joint_pos'] > (max_B + tol) or self.ob_dict['BR_coxa_joint_pos'] < (min_B - tol) else 0
        # # reward -= factor if self.ob_dict['CR_coxa_joint_pos'] > (-min_AC + tol) or self.ob_dict['CR_coxa_joint_pos'] < (-max_AC - tol) else 0
        # # reward -= factor if self.ob_dict['AL_coxa_joint_pos'] > (-min_AC + tol) or self.ob_dict['AL_coxa_joint_pos'] < (-max_AC - tol) else 0
        # # reward -= factor if self.ob_dict['BL_coxa_joint_pos'] > (max_B + tol) or self.ob_dict['BL_coxa_joint_pos'] < (min_B - tol) else 0
        # # reward -= factor if self.ob_dict['CL_coxa_joint_pos'] > (max_AC + tol) or self.ob_dict['CL_coxa_joint_pos'] < (min_AC - tol) else 0

        # fact2 = 0.0001
        # tib_fact2 = 0.04
        # if swing == 'right':
        #     # Stance foot
        #     reward -= fact2 if self.ob_dict['AL_femur_joint_pos'] < 0.1 else 0
        #     reward -= fact2 if self.ob_dict['BR_femur_joint_pos'] < 0.1 else 0
        #     reward -= fact2 if self.ob_dict['CL_femur_joint_pos'] < 0.1 else 0
        #     reward -= tib_fact2 if self.ob_dict['AL_tibia_joint_pos'] < 0.9 or self.ob_dict['AL_tibia_joint_pos'] > 1.5 else 0
        #     reward -= tib_fact2 if self.ob_dict['BR_tibia_joint_pos'] < 0.9 or self.ob_dict['BR_tibia_joint_pos'] > 1.5 else 0
        #     reward -= tib_fact2 if self.ob_dict['CL_tibia_joint_pos'] < 0.9 or self.ob_dict['CL_tibia_joint_pos'] > 1.5 else 0
        #     # Swing foot
        #     if self.steps % self.time_of_step < (0.8*self.time_of_step) or self.steps % self.time_of_step > (0.2*self.time_of_step):
        #         reward -= fact2 if self.ob_dict['AR_femur_joint_pos'] > -0.3 else 0
        #         reward -= fact2 if self.ob_dict['BL_femur_joint_pos'] > -0.3 else 0
        #         reward -= fact2 if self.ob_dict['CR_femur_joint_pos'] > -0.3 else 0
        #         reward -= tib_fact2 if self.ob_dict['AR_tibia_joint_pos'] < 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['BL_tibia_joint_pos'] < 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['CR_tibia_joint_pos'] < 1.5 else 0
        #     else:
        #         reward -= fact2 if self.ob_dict['AR_femur_joint_pos'] < 0.1 else 0
        #         reward -= fact2 if self.ob_dict['BL_femur_joint_pos'] < 0.1 else 0
        #         reward -= fact2 if self.ob_dict['CR_femur_joint_pos'] < 0.1 else 0
        #         reward -= tib_fact2 if self.ob_dict['AR_tibia_joint_pos'] < 0.9 or self.ob_dict['AR_tibia_joint_pos'] > 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['BL_tibia_joint_pos'] < 0.9 or self.ob_dict['BL_tibia_joint_pos'] > 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['CR_tibia_joint_pos'] < 0.9 or self.ob_dict['CR_tibia_joint_pos'] > 1.5 else 0
        # if swing == 'left':
        #     # Stance
        #     reward -= fact2 if self.ob_dict['AR_femur_joint_pos'] < 0.1 else 0
        #     reward -= fact2 if self.ob_dict['BL_femur_joint_pos'] < 0.1 else 0
        #     reward -= fact2 if self.ob_dict['CR_femur_joint_pos'] < 0.1 else 0
        #     reward -= tib_fact2 if self.ob_dict['AR_tibia_joint_pos'] < 0.9 or self.ob_dict['AR_tibia_joint_pos'] > 1.5 else 0
        #     reward -= tib_fact2 if self.ob_dict['BL_tibia_joint_pos'] < 0.9 or self.ob_dict['BL_tibia_joint_pos'] > 1.5 else 0
        #     reward -= tib_fact2 if self.ob_dict['CR_tibia_joint_pos'] < 0.9 or self.ob_dict['CR_tibia_joint_pos'] > 1.5 else 0
        #     # Swing
        #     if self.steps % self.time_of_step < (0.8*self.time_of_step) or self.steps % self.time_of_step > (0.2*self.time_of_step):
        #         reward -= fact2 if self.ob_dict['AL_femur_joint_pos'] > -0.3 else 0
        #         reward -= fact2 if self.ob_dict['BR_femur_joint_pos'] > -0.3 else 0
        #         reward -= fact2 if self.ob_dict['CL_femur_joint_pos'] > -0.3 else 0
        #         reward -= tib_fact2 if self.ob_dict['AL_tibia_joint_pos'] < 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['BR_tibia_joint_pos'] < 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['CL_tibia_joint_pos'] < 1.5 else 0
        #     else:
        #         reward -= fact2 if self.ob_dict['AL_femur_joint_pos'] < 0.1 else 0
        #         reward -= fact2 if self.ob_dict['BR_femur_joint_pos'] < 0.1 else 0
        #         reward -= fact2 if self.ob_dict['CL_femur_joint_pos'] < 0.1 else 0
        #         reward -= tib_fact2 if self.ob_dict['AL_tibia_joint_pos'] < 0.9 or self.ob_dict['AL_tibia_joint_pos'] > 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['BR_tibia_joint_pos'] < 0.9 or self.ob_dict['BR_tibia_joint_pos'] > 1.5 else 0
        #         reward -= tib_fact2 if self.ob_dict['CL_tibia_joint_pos'] < 0.9 or self.ob_dict['CL_tibia_joint_pos'] > 1.5 else 0
            
        # # Make sure tibia not outstretched.
        # # pre_reward = reward
        # # fact2 = 0.2
        # # reward -= fact2 if self.ob_dict['AR_tibia_joint_pos'] < 0.75 or self.ob_dict['AR_tibia_joint_pos'] > 2.0 else 0
        # # reward -= fact2 if self.ob_dict['AL_tibia_joint_pos'] < 0.75 or self.ob_dict['AL_tibia_joint_pos'] > 2.0 else 0
        # # reward -= fact2 if self.ob_dict['BR_tibia_joint_pos'] < 1.3 or self.ob_dict['BR_tibia_joint_pos'] > 2.0 else 0
        # # reward -= fact2 if self.ob_dict['BL_tibia_joint_pos'] < 1.3 or self.ob_dict['BL_tibia_joint_pos'] > 2.0 else 0
        # # reward -= fact2 if self.ob_dict['CR_tibia_joint_pos'] < 0.75 or self.ob_dict['CR_tibia_joint_pos'] > 2.0 else 0
        # # reward -= fact2 if self.ob_dict['CL_tibia_joint_pos'] < 0.75 or self.ob_dict['CL_tibia_joint_pos'] > 2.0 else 0

        # reward -= 0.5 if (self.body_xyz[2] < 0.29 or self.body_xyz[2] > 0.33) and self.steps < 50 else 0

        # # print(self.steps % self.time_of_step, 0.6*self.time_of_step)
        # # cox_rew = abs(pre_reward - reward)
        #     # print(swing, "cox penalty",abs(reward - pre_reward))

        # # if not (not self.ob_dict['AR_foot_link_left_ground'] and not self.ob_dict['BL_foot_link_left_ground'] and not self.ob_dict['CR_foot_link_left_ground']) or (not self.ob_dict['AL_foot_link_left_ground'] and not self.ob_dict['BR_foot_link_left_ground'] and not self.ob_dict['CL_foot_link_left_ground']):
        # # if (self.ob_dict['AR_foot_link_left_ground'] or self.ob_dict['BL_foot_link_left_ground'] or self.ob_dict['CR_foot_link_left_ground']) and (self.ob_dict['AL_foot_link_left_ground'] or self.ob_dict['BR_foot_link_left_ground'] or self.ob_dict['CL_foot_link_left_ground']):
        # #     reward -= 0.1
        # # # But not running (no feet on the ground)
        # # if (self.ob_dict['AR_foot_link_left_ground'] and self.ob_dict['BL_foot_link_left_ground'] and self.ob_dict['CR_foot_link_left_ground']) and (self.ob_dict['AL_foot_link_left_ground'] and self.ob_dict['BR_foot_link_left_ground'] and self.ob_dict['CL_foot_link_left_ground']):
        # #     reward -= 0.05
        # # Stop leg crossing
        # # if abs(self.ob_dict['AR_coxa_joint_pos'] - self.ob_dict['BR_coxa_joint_pos'] ) > 0.8 or abs(self.ob_dict['CR_coxa_joint_pos'] - self.ob_dict['BR_coxa_joint_pos'] ) > 0.8 :
        # #     reward -= 0.02
        # #     # print("right crossing")
        # # if abs(self.ob_dict['AL_coxa_joint_pos'] - self.ob_dict['BL_coxa_joint_pos'] ) > 0.8 or abs(self.ob_dict['CL_coxa_joint_pos'] - self.ob_dict['BL_coxa_joint_pos'] ) > 0.8 :
        # #     reward -= 0.02
        # #     # print("left crossing")
        # # print(self.ob_dict['AR_foot_link_left_ground'], self.ob_dict['BL_foot_link_left_ground'], self.ob_dict['CR_foot_link_left_ground'], self.ob_dict['AL_foot_link_left_ground'], self.ob_dict['BR_foot_link_left_ground'], self.ob_dict['CL_foot_link_left_ground'])



        # fact = 0.001
        # right_coxas = [0,9,12]
        # left_coxas = [3,6,15]
        # right_femurs = [1,10,13]
        # left_femurs = [4,7,16]
        # right_tibias = [2,11,14]
        # left_tibias = [5,8,17]
        # # It should only be the front and back legs that are paired and expected to have similar contact forces
        # reward -= fact*((actions[right_femurs[0]] - actions[right_femurs[2]])**2)
        # reward -= fact*((actions[left_femurs[0]] - actions[left_femurs[2]])**2)
        
        # reward -= fact*((actions[right_tibias[0]] - actions[right_tibias[2]])**2)
        # reward -= fact*((actions[left_tibias[0]] - actions[left_tibias[2]])**2)
        
        # reward -= fact*((actions[right_coxas[0]] + 1.0 - actions[right_coxas[2]])**2) 
        # reward -= fact*((actions[left_coxas[0]] - actions[left_coxas[2]] + 1.0)**2)
        
        # # Coxa joints are negative in symmetry.
        # # rew_before = reward
        # sym_fact = 0.0005
        # # negatives = [0,3,6,9,12,15]
        # if len(self.action_store) == (self.time_of_step):
        #     # prev_z = z = next_z = 0
        #     # if self.box_info is not None and (self.box_num+1) < len(self.box_info[0]) and self.box_num > 0:
        #         # prev_z = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        #         # z = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        #         # next_z = self.box_info[1][self.box_num+1][2] + self.box_info[2][self.box_num+1][2]
        #     # if round(z - prev_z,2) == round(next_z - z,2):
        #     reward -= sym_fact*np.sum((-1*actions[0] - self.action_store[0][3])**2)
        #     reward -= sym_fact*np.sum((-1*actions[6] - self.action_store[0][9])**2)
        #     reward -= sym_fact*np.sum((-1*actions[12] - self.action_store[0][15])**2)
        #     reward -= sym_fact*np.sum((actions[1:3] - self.action_store[0][4:6])**2)
        #     reward -= sym_fact*np.sum((actions[7:9] - self.action_store[0][10:12])**2)
        #     reward -= sym_fact*np.sum((actions[13:15] - self.action_store[0][16:18])**2)
        # self.action_store.append(actions)

        # # sym_rew = abs(reward - rew_before)
        # # print('cox_rew', cox_rew, 'sym_rew', sym_rew, 'leg_sym_rew', leg_sym_rew, 'tib_rew', tib_rew)
        
        # # "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
        # # "AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
        # # "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
        # # "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
        # # "CR_coxa_joint","CR_femur_joint","CR_tibia_joint",
        # # "CL_coxa_joint","CL_femur_joint","CL_tibia_joint"]
        # # [0,1,2]
        # # [3,4,5]
        # # [6,7,8]
        # # [9,10,11]
        # # [12,13,14]
        # # [15,16,17]
        
        return reward, done

    def get_observation(self):
        # Get joint positions and velocities
        jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
        # print(jointStates)
        # ======================================================================
        self.ob_dict.update({self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(self.ac_size)])})
        self.ob_dict.update({self.state[i+self.ac_size]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(self.ac_size)])})

        # Get relative com
        self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
        # Get angles and velocities
        roll, pitch, yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])
        prev_r, prev_p, prev_y = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
        self.ob_dict.update({'roll':roll,'pitch':pitch,'yaw':yaw})
        self.ob_dict.update({'roll_vel':self.base_rot_vel[0],'pitch_vel':self.base_rot_vel[1],'yaw_vel':self.base_rot_vel[2]})

        rot_speed = np.array(
        	[[np.cos(-yaw), -np.sin(-yaw), 0],
        	 [np.sin(-yaw), np.cos(-yaw), 0],
        	 [		0,			 0, 1]]
        )
        vx, vy, vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))  # rotate speed back to body point of view
        self.ob_dict.update({'vx':vx,'vy':vy,'vz':vz})
        
        
        # Get feet contacts
        # self.feet = ["AR_foot_link","AL_foot_link","BR_foot_link",
                    # "BL_foot_link","CR_foot_link","CL_foot_link"]
        for n in self.feet_dict:
            self.ob_dict['prev_' + n] = self.ob_dict[n]
        for n in self.feet_dict:
            self.ob_dict[n]= len(p.getContactPoints(self.Id, -1, self.feet_dict[n], -1))>0
 
        # Tripod! left/right is arbitrary compared with AR/AL etc

        self.ob_dict['left_foot_on_ground'] = self.ob_dict['AR_foot_link'] and self.ob_dict['BL_foot_link'] and self.ob_dict['CR_foot_link']
        self.ob_dict['right_foot_on_ground'] = self.ob_dict['AL_foot_link'] and self.ob_dict['BR_foot_link'] and self.ob_dict['CL_foot_link']
        
        if self.steps != 0 and self.steps % self.time_of_step == 0:
            self.ob_dict['swing_foot'] = not self.ob_dict['swing_foot']
            self.foot_change = True
            self.foot_count =np.zeros(6)
            self.zeros = np.zeros(0)
        else: 
            self.foot_change = False
            
        # if self.ob_dict['swing_foot']: stance = 'left'; swing = 'right'
        # else: stance = 'right'; swing = 'left'

        # if not self.ob_dict['ready_to_walk'] and (not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']):
            # self.ob_dict['ready_to_walk'] = True
            # self.ob_dict['prev_left_foot_left_ground'] = False
            # self.ob_dict['prev_right_foot_left_ground'] = False

        # Check if we should change swing and stance feet.
        # if self.ob_dict['ready_to_walk'] and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
            # self.ob_dict['swing_foot'] = not self.ob_dict['swing_foot']
            # self.ob_dict['impact'] = True
        # else: self.ob_dict['impact'] = False

        self.ob_dict.update({'com_z': self.body_xyz[2] - self.z_offset})

    def update_speed(self, speed):
        self.speed = speed
        return np.array([self.ob_dict[s] for s in self.state] + [self.speed])

    def dict_from_obs(self, obs):
        # print(obs.shape)
        return {s:o for s, o in zip(self.state, obs)}

    def add_disturbance(self):
        # max_disturbance = 250
        max_disturbance = 4000
        force_x = (random.random() - 0.5)*max_disturbance
        force_y = (random.random() - 0.5)*max_disturbance
        force_z = (random.random() - 0.5)*(max_disturbance/4)
        # print("applying forces", force_x, force_y, force_z)
        p.applyExternalForce(self.Id,-1,[force_x,force_y,0],[0,0,0],p.LINK_FRAME)

    def apply_forces(self, actions=None):

        roll, pitch, yaw = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        vx, vy, vz = self.ob_dict['vx'], self.ob_dict['vy'], self.ob_dict['vz']
        y_force = - (0.1 * self.Kp * self.body_xyz[1] + self.Kd * vy)
        # x_force = 0.1*self.Kd_x * (self.speed - vx)
        
        x_force = self.Kd_x * (self.speed - vx)
        # x_force = 0
        
        
        # x_force = 0
        # x_force = self.Kd_x * (-vx)

        # z_force = 0
        # z_force = 0.1*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        
        # z_force = 0.2*(self.Kp * ((0.35+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        
        # z_force = 5*(self.Kp * ((1.5+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        z_force = 0.5*(self.Kp * ((0.32+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)

        # z_force = 0.3*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        # z_force = 10*(self.Kp * ((2+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        # x_force = self.Kd * (- vx)
        # if self.up_stairs:
        #     z_force = 5*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
        p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)

        pitch_force = -self.Kp * pitch
        yaw_force = -self.Kp * yaw
        roll_force = -self.Kp * roll
        p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)


        # self.motor_names = [
        #                     "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
        #                     "AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
        #                     "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
        #                     "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
        #                     "CR_coxa_joint","CR_femur_joint","CR_tibia_joint",
        #                     "CL_coxa_joint","CL_femur_joint","CL_tibia_joint"]

        # lefts = [0,9,12]
        # rights = [3,6,15]
        if self.cur_foot == 'left':
            stance = 'right'
        else:
            stance = 'left'
        # dx = 0.05
        dx = 0.1
        # cox_max = 0.15
        # cox_min = -0.15
        # cox_max = 0.05
        # cox_min = -0.05
        # print(self.speed, stance)
        # actions = np.zeros(18)
        max_AC = 0.7
        min_AC = 0.2
        max_B = 0.3
        min_B = -0.3
        fact = 50
        # print(stance)
        if stance == 'left':
            if self.ob_dict["AR_coxa_joint_pos"] < max_AC:
                actions[0] += fact*(self.Kp_coxa/400)*(dx)
            if self.ob_dict["AL_coxa_joint_pos"] < -min_AC:
                actions[3] += fact*(self.Kp_coxa/400)*(dx)
            if self.ob_dict["BR_coxa_joint_pos"] > min_B:
                actions[6] += fact*(self.Kp_coxa/400)*(-dx)
            if self.ob_dict["BL_coxa_joint_pos"] > min_B:
                actions[9] += fact*(self.Kp_coxa/400)*(-dx)
            if self.ob_dict["CR_coxa_joint_pos"] < -min_AC:
                actions[12] += fact*(self.Kp_coxa/400)*(dx)
            if self.ob_dict["CL_coxa_joint_pos"] < max_AC:
                actions[15] += fact*(self.Kp_coxa/400)*(dx)
        if stance == 'right':
            if self.ob_dict["AR_coxa_joint_pos"] > min_AC:
                actions[0] += fact*(self.Kp_coxa/400)*(-dx)
            if self.ob_dict["AL_coxa_joint_pos"] > -max_AC:
                actions[3] += fact*(self.Kp_coxa/400)*(-dx)
            if self.ob_dict["BR_coxa_joint_pos"] < max_B:
                actions[6] += fact*(self.Kp_coxa/400)*(dx)
            if self.ob_dict["BL_coxa_joint_pos"] < max_B:
                actions[9] += fact*(self.Kp_coxa/400)*(dx)
            if self.ob_dict["CR_coxa_joint_pos"] > -max_AC:
                actions[12] += fact*(self.Kp_coxa/400)*(-dx)
            if self.ob_dict["CL_coxa_joint_pos"] > min_AC:
                actions[15] += fact*(self.Kp_coxa/400)*(-dx)
        femur_targ = 0.1
        femur_coeff = 2
        tibia_targ = 1.4
        tibia_coeff = 2
        femurs = [1,4,7,10,13,16]
        actions[1] += femur_coeff*(self.Kp/400)*-1*(self.ob_dict["AR_femur_joint_pos"] - femur_targ)
        actions[4] += femur_coeff*(self.Kp/400)*-1*(self.ob_dict["AL_femur_joint_pos"] - femur_targ)
        actions[7] += femur_coeff*(self.Kp/400)*-1*(self.ob_dict["BR_femur_joint_pos"] - femur_targ)
        actions[10] += femur_coeff*(self.Kp/400)*-1*(self.ob_dict["BL_femur_joint_pos"] - femur_targ)
        actions[13] += femur_coeff*(self.Kp/400)*-1*(self.ob_dict["CR_femur_joint_pos"] - femur_targ)
        actions[16] += femur_coeff*(self.Kp/400)*-1*(self.ob_dict["CL_femur_joint_pos"] - femur_targ)
        
        actions[2] += tibia_coeff*(self.Kp/400)*-1*(self.ob_dict["AR_tibia_joint_pos"] - tibia_targ)
        actions[5] += tibia_coeff*(self.Kp/400)*-1*(self.ob_dict["AL_tibia_joint_pos"] - tibia_targ)
        actions[8] += tibia_coeff*(self.Kp/400)*-1*(self.ob_dict["BR_tibia_joint_pos"] - tibia_targ)
        actions[11] += tibia_coeff*(self.Kp/400)*-1*(self.ob_dict["BL_tibia_joint_pos"] - tibia_targ)
        actions[14] += tibia_coeff*(self.Kp/400)*-1*(self.ob_dict["CR_tibia_joint_pos"] - tibia_targ)
        actions[17] += tibia_coeff*(self.Kp/400)*-1*(self.ob_dict["CL_tibia_joint_pos"] - tibia_targ)
        
        if self.steps != 0 and self.steps % self.time_of_step == 0:
            if self.cur_foot == 'left': self.cur_foot = 'right'
            else: self.cur_foot = 'left'
        return actions

    def save_sim_data(self, best=False, worst=False, trans=False):
        # print("saving sim data.. ", best, worst, trans)
        if trans: path = self.PATH + 'trans_'
        elif best: path = self.PATH + 'best_'
        elif worst: path = self.PATH + 'worst_'
        else: path = self.PATH
        try:
            np.save(path + 'sim_data.npy', np.array(self.sim_data))
            if path != self.PATH:
                np.save(self.PATH + 'sim_data.npy', np.array(self.sim_data))
            if (self.up_stairs or self.down_stairs or self.up_down_flat) and self.box_info is not None:
                np.save(path + 'box_info_pos.npy', np.array(self.box_info[1]))
                np.save(path + 'box_info_size.npy', np.array(self.box_info[2]))
                np.save(path + 'box_info_frict.npy', np.array(self.box_info[3]))
                if path != self.PATH:
                    np.save(self.PATH + 'box_info_pos.npy', np.array(self.box_info[1]))
                    np.save(self.PATH + 'box_info_size.npy', np.array(self.box_info[2]))
                    np.save(self.PATH + 'box_info_frict.npy', np.array(self.box_info[3]))
        except Exception as e:
            print("Save sim data error:")
            print(e)
            return

    # Function to save joint states, 
    def record_sim_data(self):
    	if len(self.sim_data) > 100000: return
    	data = [self.body_xyz, [self.qx, self.qy, self.qz, self.qw]]
    	joints = p.getJointStates(self.Id, self.motors)
    	data.append([i[0] for i in joints])
    	self.sim_data.append(data)
    
    def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None):
        pos = [pos[0], pos[1]+self.y_offset, pos[2]]
        p.resetBasePositionAndOrientation(self.Id, pos, orn)
        if joints is not None:
            if joint_vel is not None:
                for j, jv, m in zip(joints, joint_vel, self.motors):
                    p.resetJointState(self.Id, m, targetValue=j, targetVelocity=jv)
            else:
                for j, m in zip(joints, self.motors):
                	p.resetJointState(self.Id, m, targetValue=j)
        if velocities is not None:
            p.resetBaseVelocity(self.Id, velocities[0], velocities[1])

    def transform_no_rot(self, pos, point):
        trans = np.array([[1,0,0,pos[0]],[0,1,0,pos[1]],[0,0,1,pos[2]],[0,0,0,1]])
        return np.dot(trans,[point[0],point[1],point[2], 1])

    # def get_image(self, ds=[0.7, 0, -1], cams=[0.2, 0, -0.0]):
    def get_image(self, ds=[0.6, 0, -0.05], cams=[0.45, 0, -0.0]):
        camTargetPos = [0.,0.,0.]
        cameraUp = [0,0,1]
        cameraPos = [1,1,1]
        yaw = 40
        pitch = 10.0

        roll=0
        upAxisIndex = 2
        camDistance = 4
        # new ===================
        pixelWidth = 48
        pixelHeight = 48
        # nearPlane = 0.01
        # farPlane = 1000
        lightDirection = [0,1,0]
        lightColor = [1,1,1]#optional argument
        # fov = 60
        pos, orn = p.getBasePositionAndOrientation(self.Id)
        roll, pitch, yaw = p.getEulerFromQuaternion(orn)
        # dx, dy, dz = 0.7, 0, -1
        # cam_dx, cam_dy, cam_dz = 0.2, 0, -0.0
        dx, dy, dz = ds
        cam_dx, cam_dy, cam_dz = cams


        p_x, p_y, p_z, _ = self.transform_no_rot(pos, [dx,dy,dz])
        cam_x, cam_y, cam_z, _ = self.transform_no_rot(pos, [cam_dx, cam_dy, cam_dz])
        viewMatrix = p.computeViewMatrix([cam_x,cam_y,cam_z], [p_x, p_y, p_z], cameraUp)
        try:
            image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix, projectionMatrix=self.projectionMatrix, lightDirection=lightDirection,lightColor=lightColor, renderer = p.ER_TINY_RENDERER)
            # image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix=viewMatrix, renderer = p.ER_TINY_RENDERER)
            # rgb = image[2]/255.0
            rgb = image[2][:,:,:3]/255.0
            depth = np.expand_dims(image[3], axis=2)

            # if down_stairs, block off 75% of the image. It can see too far, so if any changes far in the image, the policy is affected
            if self.down_stairs:
                if random.random() < 0.5:
                    rgb[:int(48*0.4),:,:] = 0
                    depth[:int(48*0.4),:] = 0
            # re_im = rgb*255.0
            # cv2.imshow('robot_vision',re_im.astype(dtype=np.uint8))
            # cv2.waitKey(1)
            im = np.concatenate((rgb,depth), axis=2)
            return np.nan_to_num(im)

        except Exception as e:
            # print(e)
            return np.zeros([pixelWidth, pixelHeight, 4])

if __name__ == "__main__":
    from mpi4py import MPI
    import argparse
    import cv2
    parser = argparse.ArgumentParser()
    parser.add_argument('--render',default=False,action='store_true')
    parser.add_argument('--cur',default=False,action='store_true')
    parser.add_argument('--up_stairs',default=False,action='store_true')
    parser.add_argument('--down_stairs',default=False,action='store_true')
    args = parser.parse_args()
    env = Env()

    PATH = '/home/brendan/results/thing/'
    env.arguments(RENDER=args.render, PATH=PATH, cur=args.cur, record_step=False, up_stairs=args.up_stairs, down_stairs=args.down_stairs)
    env.height_coeff = 0.1

    ob = env.reset()
    i = 0
    while True:
        # print(boxes)
        actions = (np.random.rand(18)-0.5)*2

        ob, reward, done, ob_dict = env.step(actions)
        env.get_image()
        # time.sleep(0.05)
        # cv2.imshow("frame", cv2.cvtColor(image[2], cv2.COLOR_BGR2RGB))
        # cv2.imshow("frame", image)
        # cv2.waitKey(1)
        if done:
            # print(done)
            env.reset()
            # env.obstacles.create_rand_floor()
        i += 1
