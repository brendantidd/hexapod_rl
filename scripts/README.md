### What is this repository for? ###

This is going to be work towards getting RL working on the new CSIRO hexapod.
Including:
actuator model
RL agent
RL trainer and policy
For now the RL stuff is based on my 2 legged walker in Pybullet as a placeholder


### How do I get set up? ###

May or may not need openai baselines (will try to be independent of baselines, so utils.py will end up being full of openai helpers)

Pybullet
OpenMPI (set up for MPI, so will need this even if not using MPI)
OpenCV if using vision stuff
