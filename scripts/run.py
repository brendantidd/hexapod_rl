#!/usr/bin/env python3
import sys
sys.path.append('../../brendan')
import os
from mpi4py import MPI
from baselines.bench import Monitor
from baselines.common import set_global_seeds
from baselines.common.cmd_util import make_mujoco_env, mujoco_arg_parser
from baselines.common import tf_util as U
from baselines import logger
import gym
import tensorboardX
import tensorflow as tf
import argparse
import pybullet as p
import cv2
import numpy as np
import time
import mlp_policy, pposgd_simple
from env import Env
import random

def train(num_timesteps, seed, a, PATH, env):
    def policy_fn(name, ob_space, ac_space):
        return mlp_policy.MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
            # hid_size=64, num_hid_layers=2, control=a.control)
            hid_size=128, num_hid_layers=2, entropy=a.entropy, args=a)

    rank = MPI.COMM_WORLD.Get_rank()
    set_global_seeds(seed + 10000 * rank)

    # env = Monitor(env, os.path.join(logger.get_dir(), str(rank)))
    myseed = seed + 10000 * rank
    env.seed(myseed)
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)

    writer = tensorboardX.SummaryWriter(log_dir=PATH);
    # if a.up_stairs or a.down_stairs or a.speed_cur:
    # if a.up_stairs or a.down_stairs:
    #     a.enforce_weights = True
    # if a.up_stairs:
    #     pi_name = 'pi_up'
    # elif a.down_stairs:
    #     pi_name = 'pi_down'
    # else:
    # Keep all the scopes the same for now, when it comes time to combine them, we can hanlde scope names.
    pi_name = 'pi_base'
    pi = pposgd_simple.learn(env, policy_fn, PATH=PATH, writer=writer, pi_name=pi_name, args=a,
            max_timesteps=num_timesteps,
            timesteps_per_actorbatch=2048,
            # timesteps_per_actorbatch=4096,
            clip_param=0.2, entcoeff=0.0,
            optim_epochs=10,
            optim_stepsize=3e-4,
            # optim_batchsize=64,
            optim_batchsize=32,
            gamma=0.99,
            lam=0.95,
            schedule='linear',
        )
    env.close()

    # saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=pi.scope))
    # saver.save(tf.get_default_session(), PATH + 'model.ckpt')
    saver = tf.train.Saver()
    saver.save(tf.get_default_session(), PATH + 'model.ckpt')

    return pi, env

def main(a):

    if a.hpc:
        PATH = '/home/n8942251/results/' + a.alg + '/' + a.exp + '/'
    else:
        PATH = '/home/brendan/results/' + a.alg + '/' + a.exp + '/'

    # PATH = '/home/brendan/hpc-home/results/icra/test/'
    print("Currently running experiment: ", [i for i in vars(a) if getattr(a, i) == True], "with PATH: ", PATH)

    logger.configure(dir=PATH)

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.05)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,
                                            gpu_options=gpu_options), graph=None)

    env = Env()
    env.arguments(RENDER=a.render, PATH=PATH, cur=a.cur, down_stairs=a.down_stairs, up_stairs=a.up_stairs,  variable_start=a.variable_start, add_flat=a.add_flat, disturbances=a.disturbances, colour=a.colour, speed_cur=a.speed_cur, other_rewards=a.other_rewards, control=a.control, args=a)

    if not a.test:
        # train(num_timesteps=int(10e7), seed=a.seed, a=a, PATH=PATH)
        train(num_timesteps=int(10e7), seed=a.seed, a=a, PATH=PATH, env=env)
    else:
    # ==========================================================================

        name = 'pi_base'
        WEIGHTS_PATH = '/home/brendan/hpc-home/results/hex/hex/'

        pi = mlp_policy.MlpPolicy(name=name, ob_space=env.observation_space, ac_space=env.action_space,
            hid_size=128, num_hid_layers=2, args=a)
        saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=pi.scope))
        saver.restore(tf.get_default_session(), WEIGHTS_PATH + 'model.ckpt')
        print("model loaded")
        env.add_flat = True
        env.height_coeff = 0.1
        ob = env.reset()
        # im = env.get_image()
        # sym_ob = sym_env.reset()
        # sym_im = sym_env.get_image()
        # ctrl = env.speed
        # print("new speed:", env.speed)
        speeds = []
        while True:
            # action = pi.act_vis(stochastic=False, ob=ob, im=im, ctrl=ctrl)[0]
            # action = pi.act_vis(stochastic=False, ob=ob, im=im)[0]
            action = pi.act_vis(stochastic=False, ob=ob)[0]
            ob, _, done, d =  env.step(action)
            # im = env.get_image()
            print(d['AL_femur_joint_pos'], d['AR_femur_joint_pos'], d['BL_femur_joint_pos'], d['BR_femur_joint_pos'], d['CL_femur_joint_pos'], d['CR_femur_joint_pos'])
            speeds.append(d['vx'])
            if done:
                # if env.speed == 1.25: env.speed = 0.5
                ob = env.reset()
                # im = env.get_image()
                print("prev speeds", np.mean(speeds))
                speeds = []
                print("new speed:", env.speed)
                # ctrl = env.speed
            time.sleep(0.001)
            # p.resetDebugVisualizerCamera(3.0, 50, -10.0, [d['world_x'], d['world_y'], 1])
    # ==========================================================================

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--hpc', default=False, action='store_true')
    parser.add_argument('--env', default='env')
    # parser.add_argument('--variable_start', default=False, action='store_true')
    parser.add_argument('--variable_start', default=True, action='store_false')
    parser.add_argument('--vis', default=False, action='store_true')
    # parser.add_argument('--vis', default=True, action='store_false')
    parser.add_argument('--down_stairs', default=False, action='store_true')
    parser.add_argument('--up_stairs', default=False, action='store_true')
    parser.add_argument('--speed_cur', default=False, action='store_true')
    # parser.add_argument('--control', default=False, action='store_true')
    parser.add_argument('--control', default=True, action='store_false')
    parser.add_argument('--add_flat', default=False, action='store_true')
    parser.add_argument('--cur', default=False, action='store_true')
    parser.add_argument('--other_rewards', default=False, action='store_true')
    parser.add_argument('--sym', default=False, action='store_true')
    # parser.add_argument('--disturbances', default=False, action='store_true')
    parser.add_argument('--disturbances', default=True, action='store_false')
    parser.add_argument('--test', default=False, action='store_true')
    parser.add_argument('--render', default=False, action='store_true')
    parser.add_argument('--no_gpu', default=False, action='store_true')
    parser.add_argument('--entropy', default=False, action='store_true')
    parser.add_argument('--colour', default=False, action='store_true')
    parser.add_argument('--sideways', default=False, action='store_true')
    parser.add_argument('--enforce_weights', default=False, action='store_true')
    parser.add_argument('--seed', type=int, default=42)
    parser.add_argument('--alg', default='hex')
    parser.add_argument('--exp', default='')
    a = parser.parse_args()
    if a.no_gpu:
        os.environ["CUDA_VISIBLE_DEVICES"]="-1"

    main(a)
