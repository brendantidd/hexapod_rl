import pybullet as p
from obstacles import Obstacles
physicsClientId = p.connect(p.GUI)


# PATH = '/home/brendan/Dropbox/robot/mybot_ws/hexapod_rl/worlds/cave.sdf'
# PATH = '/home/brendan/subt/src/3rdparty/osrf_sim/subt_gazebo/models/indian_tunnel/model.sdf'
# PATH = '/home/brendan/subt/src/3rdparty/osrf_sim/subt_gazebo/models/gate/model.sdf'
# PATH = '/home/brendan/subt/src/3rdparty/osrf_sim/subt_gazebo/models/indian_tunnel/model.sdf'
# PATH = '/home/brendan/subt/src/3rdparty/osrf_sim/subt_gazebo/worlds/cave.world'
# PATH = '/home/brendan/subt/src/3rdparty/osrf_sim/subt_gazebo/worlds/urban_underground.world'
# print(PATH)
# p.loadSDF(PATH)
obs = Obstacles()
obs.add_stairs(length_coeff = 0.05, height_coeff = 0.01, mode='up')
while True:
    p.stepSimulation()
