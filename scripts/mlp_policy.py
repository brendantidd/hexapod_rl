from baselines.common.mpi_running_mean_std import RunningMeanStd
import baselines.common.tf_util as U
import tensorflow as tf
import gym
from baselines.common.distributions import make_pdtype
import numpy as np

class MlpPolicy(object):
    recurrent = False
    def __init__(self, name, *args, **kwargs):
        # name = 'exp_' + name
        with tf.variable_scope(name):
            self._init(*args, **kwargs)
            self.scope = tf.get_variable_scope().name

    def _init(self, ob_space, ac_space, hid_size, num_hid_layers, gaussian_fixed_var=True, entropy=False, args=None):
        assert isinstance(ob_space, gym.spaces.Box)
        # self.control = control
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        sequence_length = None

        ob = U.get_placeholder(name="ob", dtype=tf.float32, shape=[sequence_length] + list(ob_space.shape))
        # if self.control:
        #     ctrl = U.get_placeholder(name="ctrl", dtype=tf.float32, shape=[sequence_length,1])

        with tf.variable_scope("obfilter"):
            self.ob_rms = RunningMeanStd(shape=ob_space.shape)

        if self.args.vis:
            # image_size = [32,32,4]
            image_size = [48,48,4]

            im = U.get_placeholder(name="im", dtype=tf.float32, shape=[None] + image_size)
            x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
            x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
            x = U.flattenallbut0(x)
            self.vis_output = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin',
            kernel_initializer=U.normc_initializer(0.01)))

        with tf.variable_scope('vf'):
            obz = tf.clip_by_value((ob - self.ob_rms.mean) / self.ob_rms.std, -5.0, 5.0)

            last_out = obz
            if self.args.vis:
                last_out = tf.concat(axis=1,values=[last_out, self.vis_output])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
            self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,0]

        with tf.variable_scope('pol'):
            last_out = obz
            if self.args.vis:
                last_out = tf.concat(axis=1,values=[last_out, self.vis_output])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))

            if gaussian_fixed_var and isinstance(ac_space, gym.spaces.Box):
                # mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01))

                mean = tf.clip_by_value(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)),-10,10)
                if entropy:
                    logstd = tf.clip_by_value(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='logstd', kernel_initializer=tf.zeros_initializer()),-3,0)
                else:
                    logstd = tf.clip_by_value(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='logstd', kernel_initializer=tf.zeros_initializer()),-3,0)
                    # logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer())
                #logstd = tf.clip_by_value(logstd,-0.7,-1.6)

                pdparam = tf.concat([mean, mean * 0.0 + logstd], axis=1)
            else:
                pdparam = tf.layers.dense(last_out, pdtype.param_shape()[0], name='final', kernel_initializer=U.normc_initializer(0.01))

        self.pd = pdtype.pdfromflat(pdparam)

        self.state_in = []
        self.state_out = []

        # stochastic = tf.placeholder(dtype=tf.bool, shape=())
        stochastic = U.get_placeholder(name="stoch",dtype=tf.bool, shape=())
        self.ac = U.switch(stochastic, self.pd.sample(), self.pd.mode())
        if self.args.vis:
            self._act = U.function([stochastic, ob, im], [self.ac, self.vpred])
        else:
            self._act = U.function([stochastic, ob], [self.ac, self.vpred])

    def act(self, stochastic, ob):
        ac1, vpred1 =  self._act(stochastic, ob[None])
        return ac1[0], vpred1[0]
    def multi_act(self, stochastic, ob):
        ac1, vpred1 =  self._act(stochastic, ob)
        return ac1, vpred1
    def act_vis(self, stochastic, ob, im=None):
        if self.args.vis:       
            ac1, vpred1 =  self._act(stochastic, ob[None], im[None])
        else:
            ac1, vpred1 =  self._act(stochastic, ob[None])
        return ac1[0], vpred1[0]
    def multi_act_vis(self, stochastic, ob, im):
        ac1, vpred1 =  self._act(stochastic, ob, im)
        return ac1, vpred1
    def get_variables(self):
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.scope)
    def get_trainable_variables(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, self.scope)
    def get_initial_state(self):
        return []
