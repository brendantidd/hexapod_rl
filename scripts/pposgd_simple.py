from baselines.common import Dataset, explained_variance, fmt_row, zipsame
from baselines import logger
import baselines.common.tf_util as U
import tensorflow as tf, numpy as np
import time
from mpi_adam import MpiAdam
from baselines.common.mpi_moments import mpi_moments
from mpi4py import MPI
from collections import deque
import cv2
import random

def traj_segment_generator(pi, env, horizon, stochastic, args):
    t = 0
    # env.height_coeff = 0.1
    ac = env.action_space.sample() # not used, just so we have the datatype
    # ac = ac[:,'down']
    new = True # marks if we're on first timestep of an episode
    # ctrl = np.array([0.0])
    ob = env.reset()
    cur_ep_ret = 0 # return in current episode
    cur_ep_len = 0 # len of current episode
    ep_rets = [] # returns of completed episodes in this segment
    ep_lens = [] # lengths of ...

    # Initialize history arrays
    # if args.control:
    #     # ob = np.concatenate([ob, np.array([env.speed])])
    #     ob = np.concatenate([ob, np.array([0.0])])
    
    obs = np.array([ob for _ in range(horizon)])
    im = env.get_image()
    # inv_ob, inv_im = env.invert()
    imgs = np.array([im for _ in range(horizon)])

    inv_obs = np.array([ob for _ in range(horizon)])
    inv_imgs = np.array([im for _ in range(horizon)])

    rews = np.zeros(horizon, 'float32')
    vpreds = np.zeros(horizon, 'float32')
    # ctrls = np.array([ctrl for _ in range(horizon)])

    news = np.zeros(horizon, 'int32')
    acs = np.array([ac for _ in range(horizon)])
    prevacs = acs.copy()
    turn_for_image = 0
    comm = MPI.COMM_WORLD
    total_rank = comm.Get_size()
    rank = comm.Get_rank()
    t1 = time.time()
    t2 = time.time()
    times = deque(maxlen=horizon)
    while True:
        # print(t)
        prevac = ac
        # t1 = time.time()
        # ctrl = np.array([0])
        # print(ctrl)
        # if args.control:
        #     ac, vpred = pi.act_vis(stochastic, ob, im, ctrl)
        # else:
        # t1 = time.time()
        if args.vis:
            ac, vpred = pi.act_vis(stochastic, ob, im)
        else:
            ac, vpred = pi.act_vis(stochastic, ob)
        # times.append(time.time() - t1)
        # print(time.time() - t1)
        # print("time", time.time() - t1)
        ac = np.nan_to_num(ac)
        vpred = np.nan_to_num(vpred)
        # Slight weirdness here because we need value function at time T
        # before returning segment [0, T-1] so we get the correct
        # terminal value

        if t > 0 and t % horizon == 0:
            # print("thing")
            # print(np.mean(times))
            # yield {"ob" : obs, "im" : imgs, "rew" : rews, "vpred" : vpreds, "new" : news, "ac" : acs, "ctrl" : ctrls, "prevac" : prevacs, "nextvpred": vpred * (1 - new), "ep_rets" : ep_rets, "ep_lens" : ep_lens}
            yield {"ob" : obs, "im" : imgs, "inv_ob" : inv_obs, "inv_im" : inv_imgs, "rew" : rews, "vpred" : vpreds, "new" : news, "ac" : acs, "prevac" : prevacs, "nextvpred": vpred * (1 - new), "ep_rets" : ep_rets, "ep_lens" : ep_lens}
            # Be careful!!! if you change the downstream algorithm to aggregate
            # several of these batches, then be sure to do a deepcopy
            # print(env.speed)

            ep_rets = []
            ep_lens = []
            turn_for_image = 0
        i = t % horizon
        # print(ob.shape)
        obs[i] = ob
        if args.vis:
            imgs[i] = im
            # inv_obs[i] = inv_ob
            # inv_imgs[i] = inv_im
        vpreds[i] = vpred
        news[i] = new
        acs[i] = ac
        prevacs[i] = prevac
        ob, rew, new, _ = env.step(ac)
        if args.vis:
            im = env.get_image()
            # inv_ob, inv_im = env.invert()
        rews[i] = rew
        cur_ep_ret += rew
        cur_ep_len += 1
        # if args.render: time.sleep(0.01)
        if new:
            ep_rets.append(cur_ep_ret)
            ep_lens.append(cur_ep_len)
            cur_ep_ret = 0
            cur_ep_len = 0
            ob = env.reset()
            im = env.get_image()
        t += 1

def add_vtarg_and_adv(seg, gamma, lam):
    """
    Compute target value using TD(lambda) estimator, and advantage with GAE(lambda)
    """
    new = np.append(seg["new"], 0) # last element is only used for last vtarg, but we already zeroed it if last new = 1
    vpred = np.append(seg["vpred"], seg["nextvpred"])
    T = len(seg["rew"])
    seg["adv"] = gaelam = np.empty(T, 'float32')
    rew = seg["rew"]
    lastgaelam = 0
    for t in reversed(range(T)):
        nonterminal = 1-new[t+1]
        delta = rew[t] + gamma * vpred[t+1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
    seg["tdlamret"] = seg["adv"] + seg["vpred"]

def invert_act(act):
    inverted = np.empty_like(act)
    inverted[:, 4:8] = act[:, :4]
    inverted[:, :4] = act[:, 4:8]
    inverted[:, 8:10] = act[:, 10:]
    inverted[:, 10:] = act[:, 8:10]
    return inverted

def learn(env, policy_fn, *, PATH, writer, pi_name, args,
        timesteps_per_actorbatch, # timesteps per actor per update
        clip_param, entcoeff, # clipping parameter epsilon, entropy coeff
        optim_epochs, optim_stepsize, optim_batchsize,# optimization hypers
        gamma, lam, # advantage estimation
        max_timesteps=0, max_episodes=0, max_iters=0, max_seconds=0,  # time constraint
        callback=None, # you can do anything in the callback, since it takes locals(), globals()
        adam_epsilon=1e-5,
        schedule='constant' # annealing for stepsize parameters (epsilon and adam)
        ):
    # Setup losses and stuff
    # ----------------------------------------
    ob_space = env.observation_space
    ac_space = env.action_space

    pi = policy_fn(pi_name, ob_space, ac_space) # Construct network for new policy
    oldpi = policy_fn("old" + pi_name, ob_space, ac_space) # Network for old policy
    atarg = tf.placeholder(dtype=tf.float32, shape=[None]) # Target advantage function (if applicable)
    ret = tf.placeholder(dtype=tf.float32, shape=[None]) # Empirical return

    lrmult = tf.placeholder(name='lrmult', dtype=tf.float32, shape=[]) # learning rate multiplier, updated with schedule
    clip_param = clip_param * lrmult # Annealed cliping parameter epislon

    ob = U.get_placeholder_cached(name="ob")
    # if args.control:
    #     ctrl = U.get_placeholder_cached(name="ctrl")
    if args.vis:
        im = U.get_placeholder_cached(name="im")
    ac = pi.pdtype.sample_placeholder([None])
    inv_ac = pi.pdtype.sample_placeholder([None])

    # PPO1====================================================
    kloldnew = oldpi.pd.kl(pi.pd)
    # logpi = pi.pd.logp(ac)
    # print(logpi.shape)
    # ent = -pi.pd.logp(ac)
    ent = pi.pd.entropy()
    # ent = pi.pd.logp(ac)
    meankl = tf.reduce_mean(kloldnew)
    meanent = tf.reduce_mean(ent)
    if args.entropy:
        entcoeff = 0.01
    else:
        entcoeff = 0.0
    pol_entpen = (-entcoeff) * meanent
    ratio = tf.exp(pi.pd.logp(ac) - oldpi.pd.logp(ac)) # pnew / pold
    surr1 = ratio * atarg # surrogate from conservative policy iteration
    surr2 = tf.clip_by_value(ratio, 1.0 - clip_param, 1.0 + clip_param) * atarg #
    pol_surr = tf.clip_by_value(-tf.reduce_mean(tf.minimum(surr1, surr2)), -1, 1) # PPO's pessimistic surrogate (L^CLIP)
    vf_loss = tf.reduce_mean(tf.square(pi.vpred - ret))
    # Presumably the trade off between symmetry loss is exploration?
    # sym_loss = 4*tf.reduce_mean(tf.square(pi.pd.mean - inv_ac))
    if args.sym:
        print("sym coeff = ", args.sym_coeff)
        # sym_loss = 0.01*tf.reduce_mean(tf.square(pi.pd.mean - inv_ac))
        sym_loss = args.sym_coeff*tf.reduce_mean(tf.square(pi.pd.mean - inv_ac))
        total_loss = pol_surr + pol_entpen + vf_loss + sym_loss
    else:
        total_loss = pol_surr + pol_entpen + vf_loss
    std = tf.reduce_mean(pi.pd.std)
    mean_ratio = tf.reduce_mean(ratio)
    mean_adv = tf.reduce_mean(atarg)
    var_list = pi.get_trainable_variables()
    adam = MpiAdam(var_list, epsilon=adam_epsilon)
    assign_old_eq_new = U.function([],[], updates=[tf.assign(oldv, newv)
        for (oldv, newv) in zipsame(oldpi.get_variables(), pi.get_variables())])
    if args.sym:
        losses = [sym_loss, pol_surr, pol_entpen, vf_loss, meankl, meanent, std, mean_ratio, mean_adv]
        loss_names = ["sym_loss", "pol_surr", "pol_entpen", "vf_loss", "kl", "ent", "std", "ratio", "adv"]
        get_inverse_act = U.function([ob, im],[pi.pd.mean])
        lossandgrad = U.function([ob, im, ac, atarg, ret, lrmult, inv_ac], losses + [U.flatgrad(total_loss, var_list, clip_norm=40)])
        compute_losses = U.function([ob, im, ac, atarg, ret, lrmult, inv_ac], losses)
    else:
        print("---- no sym -----")
        losses = [pol_surr, pol_entpen, vf_loss, meankl, meanent, std, mean_ratio, mean_adv]
        loss_names = ["pol_surr", "pol_entpen", "vf_loss", "kl", "ent", "std", "ratio", "adv"]
        if args.vis:
            lossandgrad = U.function([ob, im, ac, atarg, ret, lrmult], losses + [U.flatgrad(total_loss, var_list, clip_norm=40)])
        else:
            lossandgrad = U.function([ob, ac, atarg, ret, lrmult], losses + [U.flatgrad(total_loss, var_list, clip_norm=40)])
        if args.vis:
            compute_losses = U.function([ob, im, ac, atarg, ret, lrmult], losses)
        else:
            compute_losses = U.function([ob, ac, atarg, ret, lrmult], losses)
    # --------------------------------------------------------------------------

    U.initialize()
    adam.sync()
    if args.hpc:
        BASE_PATH = '/home/n8942251/results/sp/a/sp_other4/'
    else:
        BASE_PATH = '/home/brendan/hpc-home/results/sp/sp_other4/'
    if args.enforce_weights:
        try:
            # saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=pi.scope))
            saver = tf.train.Saver()
            saver.restore(tf.get_default_session(), BASE_PATH + 'model.ckpt')
            print("Model weights loaded")
        except Exception as e:
            print(e)
            print("No model to load")
            if args.enforce_weights: exit()
    # try:
    #     adam.restore(BASE_PATH)
    #     print("Adam parameters restored")
    # except Exception as e:
    #     print("No adam weights to load")

    # Prepare for rollouts
    # ----------------------------------------
    seg_gen = traj_segment_generator(pi, env, timesteps_per_actorbatch, stochastic=True, args=args)

    episodes_so_far = 0
    timesteps_so_far = 0
    iters_so_far = 0
    tstart = time.time()
    t1 = time.time()
    lenbuffer = deque(maxlen=100) # rolling buffer for episode lengths
    rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards

    assert sum([max_iters>0, max_timesteps>0, max_episodes>0, max_seconds>0])==1, "Only one time constraint permitted"
    KL_TARG = 0.01
    KL_LOSS = KL_TARG
    cur_lrmult = 1.0
    ADAPTIVE_LR = True
    # ADAPTIVE_LR = False
    MAX_LR = 1.0
    all_rewards = []
    best_reward = 0
    t2 = time.time()
    t3 = time.time()

    while not args.test:

        t1 = time.time()

        if callback: callback(locals(), globals())
        if max_timesteps and timesteps_so_far >= max_timesteps:
            break
        elif max_episodes and episodes_so_far >= max_episodes:
            break
        elif max_iters and iters_so_far >= max_iters:
            break
        elif max_seconds and time.time() - tstart >= max_seconds:
            break

        if schedule == 'constant':
            cur_lrmult = 1.0
        elif schedule == 'linear':
            # if ADAPTIVE_LR:
            #     if KL_LOSS > (KL_TARG * 1.5): cur_lrmult *= 2
            #     if KL_LOSS < (KL_TARG / 1.5): cur_lrmult /= 2
            #     if cur_lrmult > MAX_LR: cur_lrmult = MAX_LR
            #     if cur_lrmult < 0.0001: cur_lrmult = 0.0001
            #     # cur_lrmult = 1.0
            #     # MAX_LR = ((0.01-1)/max_timesteps)*timesteps_so_far + 1.0
            # else: cur_lrmult =  max(1.0 - float(timesteps_so_far) / max_timesteps, 0.0001)
            cur_lrmult =  max(1.0 - float(timesteps_so_far) / max_timesteps, 0)
            # else: cur_lrmult =  max(1.0 - float(timesteps_so_far) / max_timesteps, 0)
        else:
            raise NotImplementedError


        logger.log("********** Iteration %i ************"%iters_so_far)
        # print("rest: ", time.time() - t2)
        # t3 = time.time()
        seg = seg_gen.__next__()
        # print("got data")
        add_vtarg_and_adv(seg, gamma, lam)

        # ob, im, ac, atarg, tdlamret, ctrl = seg["ob"], seg["im"], seg["ac"], seg["adv"], seg["tdlamret"], seg["ctrl"]
        ob, im, inv_ob, inv_im, ac, atarg, tdlamret = seg["ob"], seg["im"], seg["inv_ob"], seg["inv_im"], seg["ac"], seg["adv"], seg["tdlamret"]
        vpredbefore = seg["vpred"] # predicted value function before udpate
        atarg = (atarg - atarg.mean()) / atarg.std() # standardized advantage function estimate

        # d = Dataset(dict(ob=ob, im=im, ac=ac, atarg=atarg, vtarg=tdlamret, ctrl=ctrl), shuffle=not pi.recurrent)
        # d = Dataset(dict(ob=ob, im=im, inv_ob=inv_ob, inv_im=inv_im, ac=ac, atarg=atarg, vtarg=tdlamret), shuffle=not pi.recurrent)
        d = Dataset(dict(ob=ob, im=im, inv_ob=inv_ob, inv_im=inv_im, ac=ac, atarg=atarg, vtarg=tdlamret), shuffle=False)

        optim_batchsize = optim_batchsize or ob.shape[0]

        if hasattr(pi, "ob_rms"): pi.ob_rms.update(ob) # update running mean/std for policy

        assign_old_eq_new() # set old parameter values to new parameter values

        for _ in range(optim_epochs):
            losses = [] # list of tuples, each of which gives the loss for a minibatch
            for batch in d.iterate_once(optim_batchsize):
                if args.sym:
                    inv_act = get_inverse_act(batch["inv_ob"], batch["inv_im"])
                    inverted_act = invert_act(inv_act[0])
                    *newlosses, g = lossandgrad(batch["ob"], batch["im"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult, inverted_act)
                else:
                    if args.vis:
                        *newlosses, g = lossandgrad(batch["ob"], batch["im"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult)
                    else:
                        *newlosses, g = lossandgrad(batch["ob"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult)
                adam.update(g, optim_stepsize * cur_lrmult)
                losses.append(newlosses)

        losses = []
        for batch in d.iterate_once(optim_batchsize):
            if args.sym:
                inv_act = get_inverse_act(batch["inv_ob"], batch["inv_im"])
                inverted_act = invert_act(inv_act[0])
                newlosses = compute_losses(batch["ob"], batch["im"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult, inverted_act)
            else:
                if args.vis:
                    newlosses = compute_losses(batch["ob"], batch["im"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult)
                else:
                    newlosses = compute_losses(batch["ob"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult)
            losses.append(newlosses)
     
        meanlosses,_,_ = mpi_moments(losses, axis=0)
        # logger.log(fmt_row(13, meanlosses))
        for (lossval, name) in zipsame(meanlosses, loss_names):
            logger.record_tabular("loss_"+name, lossval)
            if name == "kl": KL_LOSS = lossval
        logger.record_tabular("ev_tdlam_before", explained_variance(vpredbefore, tdlamret))
        # print("calculated losses")

        lrlocal = (seg["ep_lens"], seg["ep_rets"]) # local values
        listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
        lens, rews = map(flatten_lists, zip(*listoflrpairs))
        lenbuffer.extend(lens)
        rewbuffer.extend(rews)
        logger.record_tabular("Value", np.mean(seg["vpred"]))
        logger.record_tabular("EpRewMean", np.mean(rewbuffer))
        logger.record_tabular("EpLenMean", np.mean(lenbuffer))
        logger.record_tabular("EpRewMean", np.mean(rewbuffer))
        logger.record_tabular("EpThisIter", len(lens))
        logger.record_tabular("TimeThisIter", time.time() - t1)
        logger.record_tabular("Kp", env.Kp)
        logger.record_tabular("Kp_coxa", env.Kp_coxa)
        # logger.record_tabular("Speed range", env.speed_range)
        # logger.record_tabular("Speed idx", env.speed_idx)
        logger.record_tabular("Speed", env.speed)
        # logger.record_tabular("Initial_reward", env.initial_reward)
        logger.record_tabular("Height_coeff", env.height_coeff)

        t1 = time.time()
        comm = MPI.COMM_WORLD
        if comm.Get_rank() == 0:
            writer.add_scalar("rewards", np.mean(rewbuffer), iters_so_far)
            all_rewards.append(np.mean(rewbuffer))
            np.save(PATH + 'rewards.npy',all_rewards)
            np.save(PATH + 'values.npy', seg["vpred"])
            # np.save(PATH + 'image/' + 'image_set' + str(iters_so_far), seg["im"])

            if iters_so_far % 5 == 0 and np.mean(rewbuffer) > best_reward:
            # if iters_so_far % 5 == 0:
                saver = tf.train.Saver()
                saver.save(tf.get_default_session(), PATH + 'model.ckpt')
                # adam.save(PATH)
                best_reward = np.mean(rewbuffer)

        episodes_so_far += len(lens)
        timesteps_so_far += sum(lens)
        iters_so_far += 1
        logger.record_tabular("EpisodesSoFar", episodes_so_far)
        logger.record_tabular("TimestepsSoFar", timesteps_so_far)
        logger.record_tabular("TimeElapsed", time.time() - tstart)
        if MPI.COMM_WORLD.Get_rank()==0:
            logger.dump_tabular()

    return pi

def flatten_lists(listoflists):
    return [el for list_ in listoflists for el in list_]
