
import pybullet as p
import numpy as np
import time
from OpenGL import GLU
import pylab
import sys
sys.path.append('../../brendan')
from env import Env
from assets.env_hopper import Env as Env_hopper
from assets.env_ghost import Env as Env_ghost
from assets.pb_env import PBEnv
from assets.obstacles import Obstacles
import os
import cv2

obstacles = Obstacles()

HOPPER1 = True
HOPPER1 = False

GHOST1 = True
GHOST1 = False

NB_MODELS = 1

SLOPE = False
# SLOPE = True

COLOUR = True
# COLOUR = False

best1 = True
best1 = False

worst1 = True
worst1 = False

trans1 = True
trans1 = False

best2 = True
best2 = False

worst2 = True
worst2 = False

trans2 = True
trans2 = False

#
best3 = True
best3 = False

worst3 = True
worst3 = False

trans3 = True
trans3 = False
#

# SAVE_PATH = '/home/brendan/results/images/up/'
# SAVE_PATH = '/home/brendan/results/images/down/'
# SAVE_PATH = '/home/brendan/results/images/flat/'
#
# MODEL_PATH1 = '/home/brendan/hpc-home/results/select/data1/vis/'
# MODEL_PATH1 = '/home/brendan/hpc-home/results/sp/up_speed_5/'
# MODEL_PATH1 = '/home/brendan/hpc-home/results/sp/flat_speed4/'
# MODEL_PATH1 = '/home/brendan/results/hex/hex2/'
MODEL_PATH1 = '/home/brendan/hpc-home/results/hex/hex/'
# MODEL_PATH1 = '/home/brendan/results/hex/test/'
# MODEL_PATH1 = '/home/brendan/results/hex/buthead1/'
# MODEL_PATH1 = '/home/brendan/results/select/back_from_the_dead12/'
# MODEL_PATH1 = '/home/brendan/hpc-home/results/options/options/'


files1 = os.listdir(MODEL_PATH1)
if 'door_size.npy' in files1: ob1 = True
else: ob1 = False
if 'stair_size.npy' in files1: stairs1 = True
else: stairs1 = False
if 'box_info_pos.npy' in files1: boxes1 = True
else: boxes1 = False

if HOPPER1:
    env1 = Env_hopper()
elif GHOST1:
    env1 = Env_ghost()
else:
    env1 = Env()
env1.arguments(RENDER=True, record_step=False)
# env1.reset()

if NB_MODELS > 1:
    y_offset1 = 1.5

    files2 = os.listdir(MODEL_PATH2)
    if 'door_size.npy' in files2: ob2 = True
    else: ob2 = False
    if 'stair_size.npy' in files2: stairs2 = True
    else: stairs2 = False
    if 'box_info_pos.npy' in files2: boxes2 = True
    else: boxes2 = False

    env2 = Env()
    # env2.arguments(RENDER=False, stairs=stairs2, ob=ob2, boxes=boxes2, y_offset=y_offset1, MASTER=False, record_step=False)
    env2.arguments(RENDER=False, y_offset=y_offset1, MASTER=False, record_step=False)
    # env2.reset()

if NB_MODELS > 2:
    y_offset2 = -1.5

    files3 = os.listdir(MODEL_PATH3)
    if 'door_size.npy' in files3: ob3 = True
    else: ob3 = False
    if 'stair_size.npy' in files3: stairs3 = True
    else: stairs3 = False
    if 'box_info_pos.npy' in files3: boxes3 = True
    else: boxes3 = False

    env3 = Env()
    # env3.arguments(RENDER=False, ob=ob3, stairs=stairs3, boxes=boxes3, y_offset=y_offset2, MASTER=False,record_step=False)
    env3.arguments(RENDER=False, y_offset=y_offset2, MASTER=False,record_step=False)
    # env3.reset()

def data_gen(PATH, env, ob=False, stairs=False, boxes=False, best=True, worst=False, y_offset=0, trans=False):

    obstacles = Obstacles(y_offset=y_offset)

    if best: pb = 'best_'
    elif worst: pb = 'worst_'
    elif trans: pb = 'trans_'
    else: pb = ''

    if ob:
        door_data_path = PATH + pb + 'door_data.npy'
        door_size_path = PATH + pb + 'door_size.npy'
    if stairs:
        stair_size_path = PATH + pb + 'stair_size.npy'
    if boxes:
        box_pos_path = PATH + pb + 'box_info_pos.npy'
        box_size_path = PATH + pb + 'box_info_size.npy'
        box_colour_path = PATH + pb + 'box_info_colour.npy'
        print("boxes...")
        # box_frict_path = PATH + pb + 'box_info_frict.npy'
    path = PATH + pb + 'sim_data.npy'
    print(path)
    env.reset()

    while True:
        try:
            sim_data = np.load(path)

            obj = []
            if ob:
                door_data = np.load(door_data_path)
                door_size = np.load(door_size_path)
                obj.extend(obstacles.insert_doorways(door_size, door_data))
            elif stairs:
                stair_size = np.load(stair_size_path)
                floor_nb = obstacles.insert_floor()
                stair_nb = obstacles.insert_stairs(stair_size)
                obj.extend([floor_nb, stair_nb])
            elif boxes:
                # print(obstacles.y_offset)
                # print(sim_data[0][0])
                # if len(sim_data) != 0 and sim_data[0][0][2] < 1:
                #     obj.append(obstacles.insert_line())
                # else:
                box_pos = np.load(box_pos_path)
                box_size = np.load(box_size_path)
                if os.path.isfile(box_colour_path):
                    box_colour = np.load(box_colour_path)
                    # print("box", box_colour)
                else:
                    box_colour = None
                # print(box_colour)
                # print(len(box_size))
                # print(box_pos, box_size)
                # box_frict = np.load(box_frict_path)
                # boxes = obstacles.insert_rand_boxes(0.0, 0.0, 0.0, box_pos, box_size, box_frict)
                # dif1 = (box_pos[0][2] + box_size[0][2]) - (box_pos[1][2] + box_size[1][2])
                # dif2 = (box_pos[4][2] + box_size[4][2]) - (box_pos[5][2] + box_size[5][2])
                act_idx = 1
                # if dif1 < 0 and dif2 < 0: act_idx = 3
                # elif dif1 > 0 and dif2 < 0: act_idx = 2
                # elif dif1 < 0 and dif2 > 0: act_idx = 0
                # print(box_colour)
                boxes = obstacles.insert_boxes(box_pos, box_size, colour=box_colour)
                # boxes = obstacles.insert_rand_boxes(0.0, 0.0, 0.0, box_pos, box_size)
                obj.extend(boxes)
            # else:
                # print("trying to insert line")
                # if not SLOPE and not HOPPER1: obj.extend(obstacles.insert_line())
                # if not SLOPE and not HOPPER1: obj.append(obstacles.insert_line())
            # print("trying to load")

            if len(sim_data) == 0:
                yield None
            # print(seg)
            count = 0
            for seg in sim_data:
                yield seg, count

                count += 1
                # print(count)

            # print(obj)
            for o in obj:
                p.removeBody(o)
        except Exception as e:
            print("exception")
            print(e)

data1 = data_gen(MODEL_PATH1, env1, ob=ob1, stairs=stairs1, boxes=boxes1, best=best1, worst=worst1, trans=trans1, y_offset=0)
# data1 = data_gen(MODEL_PATH1, env1, ob=ob1, stairs=stairs1, boxes=False, best=best1, y_offset=0)
if NB_MODELS > 1:
    data2 = data_gen(MODEL_PATH2, env2, ob=ob2, stairs=stairs2, boxes=boxes2, best=best2, worst=worst2, trans=trans2, y_offset=y_offset1)

if NB_MODELS > 2:
    data3 = data_gen(MODEL_PATH3, env3, ob=ob3, stairs=stairs3, boxes=boxes3, best=best3, worst=worst3, trans=trans3, y_offset=y_offset2)

if SLOPE:
    obstacles.create_rand_floor()

time.sleep(2)

# mp4log = p.startStateLogging(p.STATE_LOGGING_VIDEO_MP4,"/home/brendan/results/deep_mind.mp4")
i = 0
max_y = 0
max_yaw = 0
# str_count = p.addUserDebugText(str(0), [0,0,1], textColorRGB= [0.2,0.2,0.8],textSize=2 )
steps = p.addUserDebugText("steps", [0,0,1], textColorRGB= [0.2,0.2,0.8],textSize=2 )
prev_world_x = 0
while True:

    seg1, count = data1.__next__()
    if seg1 is not None:
        env1.set_position(seg1[0], seg1[1], seg1[2])
    else:
        env1.set_position([0, 0, 1], [0,0,0,1], np.zeros(12))
    env1.get_observation()

    # print(env1.ob_dict['AR_tibia_joint_pos'], env1.ob_dict['BR_tibia_joint_pos'], env1.ob_dict['CR_tibia_joint_pos'])
    # print(env1.ob_dict['AR_coxa_joint_pos'], env1.ob_dict['BR_coxa_joint_pos'], env1.ob_dict['CR_coxa_joint_pos'])
    # print(env1.ob_dict['AR_femur_joint_pos'], env1.ob_dict['BR_femur_joint_pos'], env1.ob_dict['CR_femur_joint_pos'])
    # env1.steps += 1
    # env1.reward(np.zeros(18))
    # vels = p.getBaseVelocity(env1.Id)
    # env1.get_observation()
    # print(env1.body_xyz[2])
    # print(vels[0][0])
    # if (action_idx == 0 and env.steps > 120 and env.steps < 180) or (action_idx == 3 and env.steps > 300):
    # elif (action_idx == 0 or action_idx == 3) and (env.steps > 220 and env.steps < 280):
    # print(act_idx)
    # act_idx = 2
    # act_idx = 0
    # if (act_idx == 0 and ((count > 0 and count < 130) or (count > 280 and count < 380) or (count > 520 and count < 580))) or (act_idx == 1 and ((count > 160 and count < 240) or (count > 420 and count < 480) or (count > 620 and count < 740))):
    #     policy = "up"
    # elif (act_idx == 0 and ((count > 160 and count < 240) or (count > 420 and count < 480) or (count > 620 and count < 740))) or (act_idx == 1 and ((count > 0 and count < 130) or (count > 280 and count < 380) or (count > 520 and count < 580))):
    #     policy = "down"
    # else:
    #     policy = "transition"
    #
    # if (act_idx == 0 and count < 100) or (act_idx == 1 and count < 120) or (act_idx == 2 and count < 80) or (act_idx == 3 and count < 140):
    #     policy = "up_down"
    # elif (act_idx==0 and count > 125 and count < 180) or (act_idx==1 and count > 140 and count < 300) or (act_idx == 2 and count > 340 and count < 550) or (act_idx==3 and count > 330 and count < 550):
    #     policy = "down"
    # elif (act_idx == 0 and count > 200 and count < 310) or (act_idx == 2 and count > 260 and count < 320):
    #     policy = "up_down"
    # elif (act_idx == 0 and count > 330 and count < 550)  or (act_idx == 1 and count > 330 and count < 550) or (act_idx == 2 and count > 100 and count < 240) or  (act_idx == 3 and count > 140 and count < 300):
    #     policy = "up"
    # else:
    # policy = "transition"
    p.removeUserDebugItem(steps)
    # p.removeUserDebugItem(str_policy)
    # xyz_pos = env1.body_xyz[0]+0.5, env1.body_xyz[1], env1.body_xyz[2]+0.5
    # xyz_pos = env1.body_xyz[0]+0.5, env1.body_xyz[1], env1.body_xyz[2]+0.5+0.2
    # steps = p.addUserDebugText(str(count), xyz_pos, textColorRGB= [0.2,0.2,0.8],textSize=2 )
    # str_policy = p.addUserDebugText(policy, xyz_pos1, textColorRGB= [0.2,0.2,0.8],textSize=2 )
    # left_foot_toe = p.getLinkState(env1.Id, env1.feet_dict["left_toe"])[0]
    # right_foot_toe = p.getLinkState(env1.Id, env1.feet_dict["right_toe"])[0]
    # left_foot_heel = p.getLinkState(env1.Id, env1.feet_dict["left_heel1"])[0]
    # right_foot_heel = p.getLinkState(env1.Id, env1.feet_dict["right_heel1"])[0]
    # env1.get_observation()
    # if not env1.ob_dict['left_foot_left_ground'] and not env1.ob_dict['right_foot_left_ground']:
    #     print(abs(left_foot_heel[0] - right_foot_heel[0]))
    # steps = p.addUserDebugText(str(count), xyz_pos1, textColorRGB= [0.2,0.2,0.8],textSize=2 )


    # print((env1.ob_dict["world_x"] - prev_world_x)*100)
    # prev_world_x = env1.ob_dict["world_x"]
    # print(env1.steps)
    # print(env1.z_offset, env1.body_xyz[2])
    # left_foot_x = p.getLinkState(env1.Id, env1.feet_dict["left_toe"])[0][0]
    # right_foot_x = p.getLinkState(env1.Id, env1.feet_dict["right_toe"])[0][0]
    # print()
    # if env1.body_xyz[1] > max_y:
    #     max_y = env1.body_xyz[1]
    # if env1.ob_dict['yaw'] > max_yaw:
    #     max_yaw = env1.ob_dict['yaw']
    # print(max_y, max_yaw)

    # im = env1.get_image()
    # # np.save(SAVE_PATH + 'image' + str(i) + '.npy', im)
    # #
    # cv2.imshow('frame', (im[:,:,:3]*255.0).astype(dtype=np.uint8))
    # cv2.waitKey(1)
    # i += 1
    time.sleep(0.01)
    if NB_MODELS > 1:
        seg2, _ = data2.__next__()
        # print(seg2)
        if seg2 is not None:
            # print(seg2)
            env2.set_position(seg2[0], seg2[1], seg2[2])
        else:
            env2.set_position([0, 0, 1], [0,0,0,1], np.zeros(12))

    if NB_MODELS > 2:
        seg3, _, _ = data3.__next__()
        if seg3 is not None:
            env3.set_position(seg3[0], seg3[1], seg3[2])
        else:
            env3.set_position([0, 0, 1], [0,0,0,1], np.zeros(12))

    # time.sleep(0.02)
    # Camera directly following robot: change seg #, and if you want constant z height
    # p.resetDebugVisualizerCamera(3.0, 50, -10.0, seg1[0])

    # p.resetDebugVisualizerCamera(3.0, 50, -10.0, [seg1[0][0], seg1[0][1], 1])

    # p.resetDebugVisualizerCamera(4.0, 30, -10.0, [seg3[0][0], seg3[0][1], 1])
    # dets = p.getDebugVisualizerCamera()
    # im = p.getCameraImage(dets[0], dets[1], dets[2], dets[3])
    # print(im[2])
    # cv2.imshow('frame', im[2])
    # cv2.waitKey(1)
