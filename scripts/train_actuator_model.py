import numpy as np
import csv
import tensorflow as tf
from actuator_model import Network
import utils as U
import matplotlib.pyplot as plt

# data = np.random.rand(100, 5)
# np.savetxt("/home/brendan/Data/data.csv", data, delimiter=",")
path = '/home/brendan/Data/data.csv'
def run():
    with open(path) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        # print(np.array(readCSV).shape)
        data = []
        for row in readCSV:
            data.append(row)
        data = np.array(data)
        # print(data.shapes)
        # dates = []
        # colors = []
        # for row in readCSV:
        #     color = row[3]
        #     date = row[0]
        #     dates.append(date)

        #     colors.append(color)

    x = np.linspace(0,20,1000)
    y = np.sin(x)
    data = np.array([x] + [y])

    name = "actuator_network"
    input_ph = tf.placeholder(name='inputs', dtype=tf.float32, shape=[None, 1]) # Empirical return
    # input_ph = tf.placeholder(name='inputs', dtype=tf.float32, shape=[None, 4]) # Empirical return
    y_ph = tf.placeholder(name='inputs', dtype=tf.float32, shape=[None]) # Empirical return
    net = Network(name, input_ph)

    indicies = np.random.permutation([_ for _ in range(len(data))])
    train_idx = indicies[:int(0.8*len(indicies))]
    test_idx = indicies[int(0.8*len(indicies)):]
    y_train = data[train_idx,-1]
    x_train = data[train_idx, :-1]
    y_test = data[test_idx,-1]
    x_test = data[test_idx, :-1]
    # print(y.shape, x.shape)
    loss_op = tf.losses.mean_squared_error(y_ph, net.pred)
    # loss_op = tf.losses.mean_squared_error(net.pred, y_ph)
    # optimizer = tf.train.AdamOptimizer(learning_rate=3e-4).minimize(loss_op)
    optimizer = tf.train.AdamOptimizer(learning_rate=0.00001).minimize(loss_op)

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.05)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,
                                            gpu_options=gpu_options), graph=None)

    train = U.function([input_ph, y_ph], [loss_op, optimizer])
    test = U.function([input_ph, y_ph], [loss_op, net.pred])

    U.initialize()
    epochs = 10
    batch_size = 128
    for _ in range(2):
        for _ in range(epochs):
            losses = []
            for b in range(len(x_train[0])//batch_size):
                loss, _ = train(x_train[b*batch_size:b*batch_size+batch_size], y_train[b*batch_size:b*batch_size+batch_size])
                losses.append(loss)
            test_loss, pred = test(x_test, y_test)
            print(np.mean(losses), test_loss)
            plt.plot(x, y, 'r', np.squeeze(x_test), pred, 'b')
            plt.show()
        # plt.plot(x, y, 'r', np.squeeze(x_test), pred, 'b')
        # plt.show()
    _, pred = test(x_test, y_test)
    plt.plot(x, y, 'r', np.squeeze(x_test), pred, 'b')
    plt.show()

if __name__=="__main__":
    run()
